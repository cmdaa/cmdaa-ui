# CMDAA UI

For development, use sbt:

    $ sbt
    [info] welcome to sbt....

    sbt:cmdaa-ui> dev
    [info] Starting webpack-dev-server...

Please create the file, "webpack/user.config.js" with contents similar to the'
following:

    module.exports = {
      serviceDN: "SERVICE_DN"
    }

The correct value of "SERVICE_DN" is the answer that was given to Helm when
setting up CMDAA.

If Tilt, or something similar, can be made to work for development again, this
requirement could go away. For now though, we'll run the CMDAA UI outside the
Kubernetes cluster and have our Webpack Dev Server setup a reverse proxy to the
service we're talking to.

To build for production simply:

    sbt build
    ./build-prod-image.sh

The `build-prod-image.sh` script is currently setup to target ':latest'. Change
it or copy-paste-change the target to get something else.
