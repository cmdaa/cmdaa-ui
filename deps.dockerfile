FROM cmdaa/sbt-nodejs

WORKDIR /src
COPY . /src

RUN sbt fastOptJS::webpack
