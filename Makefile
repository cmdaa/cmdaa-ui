NAME = cmdaa/cmdaa-ui
TAG  = 0.0.23

all: build/cmdaa-ui-opt.js

build/cmdaa-ui-opt.js: $(shell find src/main/scala/ -name "*.scala")
	sbt build

image: all
	-docker rmi $(NAME):$(TAG)
	 docker build --tag $(NAME):$(TAG) .

webpack/user.config.js:
	@./generate-user-config.sh "$(shell hostname)"

clean:
	sbt clean
	rm -rf build/
