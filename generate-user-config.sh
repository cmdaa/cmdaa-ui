#!/bin/bash

#
# Generate a webpack user config file
#
#  Usage: generate-user-config.sh %service-dn%
#
ROOT="$(dirname "$0")"
DEST="webpack/user.config.js"

if [[ $# -ne 1 ]]; then
  echo
  echo "*** Missing 'service-dn'"
  echo
  echo "*** Usage: $(basename "$0") %service-dn%"
  echo
  exit -1
fi

DN="$1"

echo ">>> Generate $DEST with DN[ $DN ] ..."
cat <<EOF > "$ROOT/$DEST"
module.exports = {
  serviceDN: "$DN"
}
EOF
