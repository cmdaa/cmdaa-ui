// ScalaJS 1.7.0
addSbtPlugin("org.scala-js"  % "sbt-scalajs" % "1.7.0")

// Only works with WebPack 4. Not looking super maintained....
addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.20.0")
