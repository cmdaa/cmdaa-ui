enablePlugins(ScalaJSBundlerPlugin)

lazy val baseNexusUrl = "https://52.72.213.202/"

organization := "io.cmdaa"
name         := "cmdaa-ui"
version      := "0.0.23-SNAPSHOT"
scalaVersion := "2.13.3"

resolvers ++= Seq(
  Resolver.mavenLocal,
  "CMDAA Nexus" at baseNexusUrl + "repository/maven-public/"
)

publishTo := {
  val nexus = baseNexusUrl+"repository/maven-"

  if (isSnapshot.value)
    Some(("CMDAA Snapshots Nexus" at nexus+"snapshots")
      .withAllowInsecureProtocol(true))
  else
    Some(("CMDAA Releases Nexus" at nexus+"releases")
      .withAllowInsecureProtocol(true))
}

Compile / npmDependencies ++= Seq(
  "bulma" -> "0.9.1",
  "@fortawesome/fontawesome-free" -> "5.15.2"
)

Compile / npmDevDependencies ++= Seq(
  "file-loader"         -> "6.2.0",
  "style-loader"        -> "2.0.0",
  "css-loader"          -> "5.2.7",
  "sass-loader"         -> "10.2.0",
  "sass"                -> "1.42.1",
  "html-webpack-plugin" -> "4.5.2",
  "copy-webpack-plugin" -> "6.4.1",
  "webpack-merge"       -> "5.8.0"
)

libraryDependencies ++= Seq(
  "io.cmdaa.grokstore"       %%% "cmdaa-gs-api"    % "0.0.23-SNAPSHOT",
  "io.cmdaa.auth"            %%% "cmdaa-auth-api"  % "0.0.23-SNAPSHOT",
  "com.thoughtworks.binding" %%% "binding"         % "12.0.0",
  "com.thoughtworks.binding" %%% "futurebinding"   % "12.0.0",
  "org.lrng.binding"         %%% "html"            % "1.0.3",

  "org.scalatest" %%% "scalatest" % "3.2.0" % Test
)

scalacOptions ++= Seq("-Ymacro-annotations", "-deprecation")

webpack               / version := "4.46.0"
startWebpackDevServer / version := "3.11.2"

webpackResources := baseDirectory.value / "webpack" * "*"

fastOptJS / webpackConfigFile := Some(baseDirectory.value / "webpack" / "webpack-fastopt.config.js")
fullOptJS / webpackConfigFile := Some(baseDirectory.value / "webpack" / "webpack-opt.config.js")
Test      / webpackConfigFile := Some(baseDirectory.value / "webpack" / "webpack-core.config.js")

fastOptJS / webpackDevServerExtraArgs := Seq(
  "--inline", "--hot", "--host", "0.0.0.0"
)
fastOptJS / webpackDevServerPort := 9000
fastOptJS / webpackBundlingMode  := BundlingMode.LibraryOnly()

Test / requireJsDomEnv := true

addCommandAlias("dev",   ";fastOptJS::startWebpackDevServer;~fastOptJS")
addCommandAlias("build", "fullOptJS::webpack")
