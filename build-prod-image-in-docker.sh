#!/bin/bash
#
# Usage:
#   build-prod-image-in-docker.sh [TAG] [DN]
#
#     TAG : image tag [0.0.23]
#     DN  : DNS domain name for services [rke.cmdaa.io]
#
IMAGE="cmdaa/cmdaa-ui"
  TAG="${1:-0.0.23}"
   DN="${2:-rke.cmdaa.io}"

docker build \
  --file Dockerfile.build \
  --build-arg SERVICE_DN=${DN} \
  --tag  ${IMAGE}:${TAG} \
    .
