/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import com.thoughtworks.binding.Binding
import org.lrng.binding.html.{NodeBinding, NodeBindingSeq}

import scala.language.implicitConversions
import scala.scalajs.js

package object utils {
  /**
   * Removes false errors in IntelliJ regarding @html methods that return a
   * Binding/Node.
   */
  implicit def intellijBindingHelper[T <: org.scalajs.dom.raw.Node](
    x: scala.xml.Node
  ): NodeBinding[T] =
    throw new AssertionError("This should never execute.")

  /** Removes fakse errors in IntelliJ. */
  implicit def intellijNodeSeqHelper[T <: org.scalajs.dom.raw.Node](
    x: scala.xml.NodeBuffer
  ): NodeBindingSeq[T] =
    throw new AssertionError("This should never execute.")

  /**
   * Removes false errors in IntelliJ regarding @html methods that return a
   * Binding/BindingSeq/Node
   */
  implicit def intellijNodeBindingSeqHelper[T <: org.scalajs.dom.raw.Node](
    x: Binding[scala.xml.NodeBuffer]
  ): Binding[NodeBindingSeq[T]] =
    throw new AssertionError("This should never execute.")

  /** Converts the standard string Date format into millisSinceEpoch */
  def strDt2millis(str: String): Double =
    js.Date.parse(str)

  /** Converts the standard string Date format into Display Date String */
  def strDt2display(str: String): String =
    dtMillis2display(strDt2millis(str))

  /** Visual Date Formatter for "js.Date". */
  def date2display(date: js.Date): String = {
    date.toLocaleDateString()+" "+date.toLocaleTimeString()
  }

  /** Visual Date Formatter for datetimes in millis */
  def dtMillis2display(millis: Double): String = {
    date2display(new js.Date(millis))
  }
}
