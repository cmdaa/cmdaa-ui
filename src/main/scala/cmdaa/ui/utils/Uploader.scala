/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.utils

import org.scalajs.dom
import org.scalajs.dom.ext.Ajax.InputData
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.{ErrorEvent, ProgressEvent}

import scala.concurrent.{Future, Promise}
import scala.scalajs.js

object Uploader {

  /** Utility for HTTP POST requests. */
  def post (
    url: String,
    data: InputData = null,
    timeout: Int = 0,
    headers: Map[String, String] = Map.empty,
    withCredentials: Boolean = false,
    responseType: String = "",
    callbacks: FileUploadCallbacks = FileUploadCallbacks()
  ): Future[dom.XMLHttpRequest] = {
    apply("POST",
      url, data, timeout, headers,
      withCredentials, responseType, callbacks
    )
  }

  /** Utility for HTTP Put requests. */
  def put (
    url: String,
    data: InputData = null,
    timeout: Int = 0,
    headers: Map[String, String] = Map.empty,
    withCredentials: Boolean = false,
    responseType: String = "",
    callbacks: FileUploadCallbacks = FileUploadCallbacks()
  ): Future[dom.XMLHttpRequest] = {
    apply("PUT",
      url, data, timeout, headers,
      withCredentials, responseType, callbacks
    )
  }

  /** Utility for sending file upload request compatible with tracking */
  def apply (
    method: String,
    url: String,
    data: InputData,
    timeout: Int,
    headers: Map[String, String],
    withCredentials: Boolean,
    responseType: String,
    callbacks: FileUploadCallbacks
  ): Future[dom.XMLHttpRequest] = {
    val req = new dom.XMLHttpRequest()

    req.upload.onloadstart = callbacks.onLoadStart
    req.upload.onprogress  = callbacks.onProgress
    req.upload.onabort     = callbacks.onAbort
    req.upload.onerror     = callbacks.onError
    req.upload.onload      = callbacks.onLoad
    req.upload.ontimeout   = callbacks.onTimeOut
    req.upload.onloadend   = callbacks.onLoadEnd

    val promise = Promise[dom.XMLHttpRequest]()

    req.onreadystatechange = { (_: dom.Event) =>
      if (req.readyState == 4) {
        if ((req.status >= 200 && req.status < 300) || req.status == 304)
          promise.success(req)
        else
          promise.failure(AjaxException(req))
      }
    }

    req.open(method, url, async = true)
    req.responseType = responseType
    req.timeout = timeout
    req.withCredentials = withCredentials

    headers.foreach(x => req.setRequestHeader(x._1, x._2))

    if (data == null) req.send() else req.send(data)
    promise.future
  }
}

/** File upload callback functions */
case class FileUploadCallbacks (
  onLoadStart: js.Any        => _ = (_: js.Any)        => {},
  onProgress:  ProgressEvent => _ = (_: ProgressEvent) => {},
  onAbort:     js.Any        => _ = (_: js.Any)        => {},
  onLoadEnd:   ProgressEvent => _ = (_: ProgressEvent) => {},
  onError:     ErrorEvent    => _ = (_: ErrorEvent)    => {},
  onLoad:      js.Any        => _ = (_: js.Any)        => {},
  onTimeOut:   js.Any        => _ = (_: js.Any)        => {}
)
