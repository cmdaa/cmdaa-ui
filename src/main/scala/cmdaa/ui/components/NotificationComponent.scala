/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.components

import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.state.NotificationActions.RemoveNotificationAction
import cmdaa.ui.state.{Notification, NotificationType}
import cmdaa.ui.utils.{dtMillis2display, intellijBindingHelper}
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Node
import org.scalajs.dom.raw.Event

object NotificationComponent {
  @html
  def component(notif: Notification)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val notifTypeClass = notif.notifType match {
      case NotificationType.Success => "is-success"
      case NotificationType.Warning => "is-warning"
      case NotificationType.Error   => "is-danger"
    }

    <div class={s"notification $notifTypeClass"}>
      <button class="delete" onclick={ e => onRemNotification(e, notif) }></button>
      <span style="float: right">{dtMillis2display(notif.posted.toDouble)}</span>
      <p class="has-text-weight-bold is-size-4 is-family-secondary">
        { notif.title}
      </p>
      <p class="is-family-monospace">
        { notif.content.bind }
      </p>
    </div>
  }

  private def onRemNotification(event: Event, notif: Notification)
    (implicit dispatcher: Dispatcher)
  : Unit = {
    event.preventDefault()
    dispatcher dispatch RemoveNotificationAction(notif)
  }
}
