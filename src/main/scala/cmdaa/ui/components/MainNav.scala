/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.components

import cmdaa.ui.AppActions.{LogoutAction, MainNavOpen}
import cmdaa.ui.AppStateRO
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.router.navLink
import cmdaa.ui.state.UnauthenticatedActions.{StartLogin, StartSignup}
import cmdaa.ui.state.{AuthenticatedStateRO, UnauthenticatedStateRO}
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Event
import org.scalajs.dom.raw._

/**
 * The primary or "main" navigation component for the CMDAA-UI Application.
 */
object MainNav {
  /**
   * The "main" nav component for CMDAA-UI Application. Responsible for
   * informing the user of the currently selected page and providing elements
   * allowing the selection of a new page.
   *
   * @param appState   the current application state
   * @param dispatcher the Application's Action/Event Dispatcher
   *
   * @return the main/primary navigation component for the CMDAA-UI Application
   */
  @html
  def component(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    // Responsive Design support for an openable/collapsible menu.
    val mainNavOpen_? = appState.mainNavOpen_?.asInstanceOf[Var[Boolean]]

    // The CSS Class representing the current state (opend/closed) of the menu.
    val activeClass = Binding { if (mainNavOpen_?.bind) "is-active" else "" }

    <nav
      class="navbar"
      data:role="navigation"
      data:aria-label="main navigation"
    >
      <div class="navbar-brand">
        <a
          href="/" class="navbar-item"
          onclick={ navLink("/")(_) }
        >
          <span class="icon">
            <i class="fab fa-superpowers"/>
          </span>
          <span>CMDAA-UI</span>
        </a>
        <a
          href="#"
          class={s"navbar-burger burger ${activeClass.bind}"}
          data:role="button"
          data:aria-label="menu"
          data:aria-expanded={s"${mainNavOpen_?.bind}"}
          data:data-target="mainNavbar"
          onclick={ e: Event =>
            e.preventDefault()
            dispatcher dispatch MainNavOpen(!mainNavOpen_?.value)
          }
        >
          <span data:aria-hidden="true"/>
          <span data:aria-hidden="true"/>
          <span data:aria-hidden="true"/>
        </a>
      </div>
      <div
        id="mainNavbar"
        class={s"navbar-menu ${activeClass.bind}"}
      >
        <div class="navbar-start">
          <!--a
            href="#" class="navbar-item"
            onclick={navLink("/docs")(_)}
          >
            <span class="icon">
              <i class="fas fa-book"/>
            </span>
            <span>Documentation</span>
          </a-->
        </div>
        <div class="navbar-end">
          <div class="navbar-item">
            {
              /*
                Authentication Buttons at "navbar-end".
                Login/Signup if unauthenticated. Logout if authenticated.
               */
              appState.authState.bind.fold (
                unauthState => unauthButtons(unauthState),
                authedState => authedButtons(authedState)
              ).bind
            }
          </div>
        </div>
      </div>
    </nav>
  }

  /**
   * Internal MainNav component for displaying the Unauthenticated set of
   * "Authentication Buttons.
   *
   * @param unauthState the Unauthenticated Application State
   * @param dispatcher  the Action Dispatcher
   * @return the elements for the buttons
   */
  @html
  private def unauthButtons(unauthState: UnauthenticatedStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <div class="buttons">
      <a
        href="#" class="button is-primary"
        onclick={ e: Event =>
          e.preventDefault()
          dispatcher dispatch StartSignup
        }
      >
        <span class="icon"><i class="fas fa-user-plus"/></span>
        <span>Sign Up</span>
      </a>
      <a
        href="#" class="button is-light"
        onclick={ e: Event =>
          e.preventDefault()
          dispatcher dispatch StartLogin
        }
      >
        <span class="icon"><i class="fas fa-sign-in-alt"/></span>
        <span>Login</span>
      </a>
    </div>

  /**
   * Buttons to display while the App is in an Authenticated State
   *
   * @param authedState the Authenticated Application State
   * @param dispatcher  the Action Dispatcher
   * @return the elements for the buttons
   */
  @html
  private def authedButtons(authedState: AuthenticatedStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <div class="buttons">
      <a
        href="#" class="button is-light"
        onclick={ e: Event =>
          e.preventDefault()
          dispatcher dispatch LogoutAction
        }
      >
        <span class="icon"><i class="fas fa-sign-out-alt" /></span>
        <span>Logout</span>
      </a>
    </div>
}
