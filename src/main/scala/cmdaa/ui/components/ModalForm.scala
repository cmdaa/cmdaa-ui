/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.components

import cmdaa.ui.dispatcher.{Dispatcher, StateAction}
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Event
import org.scalajs.dom.raw._

/** Modal Form component with "submit" and "cancel" buttons */
object ModalForm {

  /**
   * Constructs the Modal Form component with "submit" and "cancel" buttons.
   *
   * @param formId     the HTML "ID" of the form element to submit
   * @param title      the Modal's "title"
   * @param affirmText text for the "Submit" button
   * @param isOpen_?   whether or not the Modal is Open.
   * @param openAction the Action used to Open/Close the Modal
   * @param isWide     whether the component is wide
   * @param content    the Content elements for the Modal to display
   * @param dispatcher the Action Dispatcher
   *
   * @return the constructed Modal element binding
   */
  @html
  def component (
    formId:     String,
    title:      String,
    affirmText: String,
    isOpen_? :  Binding[Boolean],
    openAction: Boolean => StateAction,
    isWide:     Boolean = false
  )(content: NodeBinding[Node])(implicit
    dispatcher: Dispatcher
  ): NodeBinding[Node] = {
    val activeClass = Binding { if (isOpen_?.bind) "is-active" else "" }
    val sizeClass = if (isWide) "is-wide" else ""

    <div class={s"modal ${activeClass.bind} $sizeClass"}>
      <div class="modal-background" />
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">{title}</p>
          <button
            class="delete" data:aria-label="close"
            onclick={ _: Event => dispatcher dispatch openAction(false) }
          ></button>
        </header>
        <section class="modal-card-body">
          { content.bind }
        </section>
        <footer class="modal-card-foot">
          <input
            class="button is-success"
            type="submit"
            data:form={ formId }
            value={ affirmText }
          />
          <button
            class="button"
            onclick={ _: Event => dispatcher dispatch openAction(false) }
          >Cancel</button>
        </footer>
      </div>
    </div>
  }
}
