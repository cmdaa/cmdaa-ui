/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.components

import cmdaa.ui.dispatcher.{Dispatcher, StateAction}
import cmdaa.ui.forms.handleInput
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding._
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.raw._

case class FormMsg(text: String, status: String)

object FormInput {

  @html
  def textField (
    id:      String,
    label:   String,
    value:   Binding[String],
    action:  String => StateAction,
    formMsg: BindingSeq[FormMsg],

    fieldType: String = "text",
    required:  Boolean = false,

    placeholder: Option[String]  = None,
    minLength:   Option[Int]     = None,
    maxLength:   Option[Int]     = None,

    pattern:   Binding[Option[String]] = Var(None),
    leftIcon:  Binding[Option[String]] = Var(None),
    rightIcon: Binding[Option[String]] = Var(None),

    readonly: Binding[Boolean] = Var(false)
  )(
    implicit dispatcher: Dispatcher
  ): NodeBinding[Node] = {
    val status = Binding {
      formMsg.all.bind.headOption
        .map(_.status).getOrElse("")
    }

    <div class="field">
      <label class="label">{ label }</label>
      <div class="control has-icons-left has-icons-right">
        <input
          id={ id }
          type={ fieldType }
          class={ s"input ${status.bind}" }
          placeholder={ placeholder }
          value={ value.bind }
          oninput={ handleInput(action)(_) }
          minlength={ minLength.map(_.toString) }
          maxlength={ maxLength.map(_.toString) }
          required={ if (required) Some("required") else None }
          pattern={ pattern.bind }
          readonly={ if (readonly.bind) Some("readonly") else None }
        />
        {
          for (liClass <- Vars(leftIcon.bind.toList: _*)) yield
            <span class="icon is-small is-left"><i class={ liClass } /></span>
        } {
          for (riClass <- Vars(rightIcon.bind.toList: _*)) yield
            <span class="icon is-small is-right"><i class={ riClass } /></span>
        }
      </div>
      {
        for (msg <- formMsg) yield
          <p class={s"help ${msg.status}"}>{ msg.text }</p>
      }
    </div>
  }

  @html
  def textActionField (
    id:      String,
    label:   String,
    value:   Binding[String],
    actLbl:  Binding[String],
    formMsg: BindingSeq[FormMsg],

    inpAction: String => StateAction,
    fldAction: String => StateAction,

    fieldType: String = "text",
    required:  Boolean = false,

    placeholder: Option[String]  = None,
    minLength:   Option[Int]     = None,
    maxLength:   Option[Int]     = None,

    pattern:   Binding[Option[String]] = Var(None),
    leftIcon:  Binding[Option[String]] = Var(None),
    rightIcon: Binding[Option[String]] = Var(None)
  )(
    implicit dispatcher: Dispatcher
  ): NodeBinding[Node] = {
    val status = Binding {
      formMsg.all.bind.headOption
        .map(_.status).getOrElse("")
    }

    <div class="field">
      <label class="label">{ label }</label>
      <div class="field-body">
        <div class="field has-addons">
          <div class="control has-icons-left has-icons-right is-expanded">
            <input
              id={ id }
              type={ fieldType }
              class={ s"input ${status.bind}" }
              placeholder={ placeholder }
              value={ value.bind }
              oninput={ handleInput(inpAction)(_) }
              minlength={ minLength.map(_.toString) }
              maxlength={ maxLength.map(_.toString) }
              required={ if (required) Some("required") else None }
              pattern={ pattern.bind }
              onkeydown={ event: KeyboardEvent =>
                if (event.keyCode == 13)
                  handleAction(fldAction, value)(event)
              }
            />
            {
              for (liClass <- Vars(leftIcon.bind.toList: _*)) yield
                <span class="icon is-small is-left">
                  <i class={ liClass } />
                </span>
            } {
              for (riClass <- Vars(rightIcon.bind.toList: _*)) yield
                <span class="icon is-small is-right">
                  <i class={ riClass } />
                </span>
            }
          </div>
          <div class="control">
            <a
              class="button is-info"
              href="#"
              onclick={ handleAction(fldAction, value)(_) }
            >{ actLbl.bind }</a>
          </div>
          {
            for (msg <- formMsg) yield
              <p class={s"help ${msg.status}"}>{ msg.text }</p>
          }
        </div>
      </div>
    </div>
  }

  def handleAction[T]
    (action: T => StateAction, param: Binding[T])(event: Event)
    (implicit dispatcher: Dispatcher)
  : Unit = {
    event.preventDefault()
    event.stopImmediatePropagation()
    dispatcher dispatch action(param.asInstanceOf[Var[T]].value)
  }

  @html
  def doubleField (
    id:      String,
    label:   String,
    value:   Binding[Double],
    action:  String => StateAction,
    formMsg: BindingSeq[FormMsg],

    fieldType: String = "number",
    required:  Boolean = false,

    placeholder: Option[String] = None,
    min:         Option[Double] = None,
    max:         Option[Double] = None,
    step:        Option[Double] = None,

    leftIcon:  Binding[Option[String]] = Var(None),
    rightIcon: Binding[Option[String]] = Var(None)
  )(
    implicit dispatcher: Dispatcher
  ): NodeBinding[Node] = {
    val status = Binding {
      formMsg.all.bind.headOption
        .map(_.status)
        .getOrElse("")
    }

    <div class="field">
      <label class="label">{ label }</label>
      <div class="control has-icons-left has-icons-right">
        <input
          id={ id }
          type={ fieldType }
          class={ s"input ${status.bind}".trim }
          placeholder={ placeholder }
          value={ ""+value.bind }
          oninput={ handleInput(action)(_) }
          max={ max.map(_.toString) }
          min={ min.map(_.toString) }
          step={ step.map(_.toString) }
          required={ if (required) "required" else "false" }
        />
        {
          for (liClass <- Vars(leftIcon.bind.toList: _*)) yield
            <span class="icon is-small is-right">
              <i class={ liClass } />
            </span>
        } {
          for (riClass <- Vars(rightIcon.bind.toList: _*)) yield
            <span class="icon is-small is-right">
              <i class={ riClass } />
            </span>
        }
      </div>
      {
        for (msg <- formMsg) yield
          <p class={s"help ${msg.status}"}>{ msg.text }</p>
      }
    </div>
  }
}
