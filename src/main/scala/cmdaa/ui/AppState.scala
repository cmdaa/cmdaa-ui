/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import cmdaa.ui.dao.{AuthDao, ServiceErrResp, SessionTokenDao}
import cmdaa.ui.dispatcher._
import cmdaa.ui.state.NotificationActions.PushNotificationAction
import cmdaa.ui.state._
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.SessionToken
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.{Node, window}

/**
 * The mutable Application State that is maintained by ActionHandlers through
 * the dispatch of StateActions.
 *
 * @param route         The current route as indicated by the browser's URI path
 * @param mainNavOpen_? Whether or not the MainNav Menu is open (tablet/phone)
 * @param authState     State for whether the Application Either has an
 *                      authenticated user, or it doesn't.
 */
case class AppState (
  route: Var[String],
  mainNavOpen_? : Var[Boolean] = Var(false),

  authState: Var[Either[UnauthenticatedState, AuthenticatedState]] =
    Var(Left(UnauthenticatedState())),

  notifState: NotificationState = NotificationState()
) extends StateModel[AppStateRO] {
  private val authStateRO: Binding [
    Either[UnauthenticatedStateRO, AuthenticatedStateRO]
  ] = Binding {
    authState.bind.fold(a => Left(a.toRO), b => Right(b.toRO))
  }

  override def toRO: AppStateRO =
    AppStateRO(route, mainNavOpen_?, authStateRO, notifState.toRO)
}

/**
 * Represents the ReadOnly Application State that is distributed to components.
 *
 * @param route         The current route as indicated by the brower's URI path
 * @param mainNavOpen_? Whether or not the MainNav Menu is open (tablet/phone)
 * @param authState     State for whether the Application Either has an
 *                      authenticated user, or it doesn't.
 */
case class AppStateRO (
  route: Binding[String],
  mainNavOpen_? : Binding[Boolean],
  authState: Binding[Either[UnauthenticatedStateRO, AuthenticatedStateRO]],

  notifState: NotificationStateRO
) extends StateModelRO

object AppActions {
  /** Action for notification of updates to the brower's URI path */
  object PathUpdate extends StateAction

  /** Action for logging a User out of the Application */
  case object LogoutAction extends StateAction

  /** Action for opening/closing of the MainNav Menu for tablets/phones */
  case class MainNavOpen(value: Boolean)
  extends StateAction

  /** Action for Logging a User into the Application */
  case class LoginAction(sessToken: SessionToken)
  extends StateAction

  /** Signals the application that an Ajax Error has occurred  */
  case class AjaxErrorSignal(ajaxEx: AjaxException)
  extends StateAction

  /** Signals the app that an Service Error has occurred */
  case class ServiceErrSignal(resp: ServiceErrResp)
  extends StateAction

  /**
   * Signals the app that the user's PKI certificate is invalid. This happens
   * when certificate validation has failed.
   */
  case class InvalidPkiSignal(resp: ServiceErrResp)
  extends StateAction

  /** Signals the app that an invalid response was received from the services */
  case class BadRespSignal(resp: ServiceErrResp)
  extends StateAction
}

/**
 * Handles Application-Wide Events
 *
 * @param appState the Application State to Modify
 */
class AppActionHandler (
  private val appState: AppState
) extends ActionHandler {
  import AppActions._

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override def subHandlers: List[ActionHandler] = List (
    appState.authState.value.fold (
      unauthState => new UnauthenticatedActionHandler(unauthState),
      authState   => new AuthenticatedActionHandler(authState),
    ),
    new NotificationActionHandler(appState.notifState)
  )

  override def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case PathUpdate   => hndlPathUpdate()
    case LogoutAction => hndlLogout()

    case MainNavOpen(newState)   => hndlMainNavUpdate(newState)
    case LoginAction(sessToken)  => hndlLogin(sessToken)


    // Error Handler
    case AjaxErrorSignal(ajaxEx) => hndlAjaxErr(dispatcher, ajaxEx)
    case ServiceErrSignal(resp)  => hndlSvcErr(dispatcher, resp)
    case InvalidPkiSignal(resp)  => hndlInvalidPki(dispatcher, resp)
    case BadRespSignal(resp)     => hndlBadResp(dispatcher, resp)

    case _ =>
  }

  /** Handles UI routing from URI updates */
  private def hndlPathUpdate(): Unit = {
    appState.route.value = window.location.pathname
    appState.mainNavOpen_?.value = false
  }

  /** Handles opening/closing of the MainNav menu */
  private def hndlMainNavUpdate(newState: Boolean): Unit = {
    appState.mainNavOpen_?.value = newState
  }

  /**
   * Establishes the currently used SessionToken of the recently authenticated
   * user.
   *
   * @param sessToken the SessionToken to store and use for authenticated
   *                  access to CMDAA APIs
   */
  private def hndlLogin(sessToken: SessionToken): Unit = {
    appState.authState.value = Right(AuthenticatedState(sessToken))
    SessionTokenDao.save(sessToken)
  }

  /** Handles user logout. */
  private def hndlLogout(): Unit = {
    SessionTokenDao.load() foreach { token =>
      AuthDao.logout(token) onComplete { t_res =>
        // Success or fail - Put the client in an unauthenticated state
        appState.authState.value = Left(UnauthenticatedState())
        SessionTokenDao.rmToken()

        // Handle errors/responses
        t_res.fold({ t: Throwable =>
          println(s"Error logging out: ${t.getClass.getName} - ${t.getMessage}")
        }, {
          _.fold({ errResp =>
            println(s"Error logging out: $errResp")
          }, { okResp =>
            println(s"Logged out: $okResp")
          })
        })
      }
    }
  }

  /** Handles Ajax Errors */
  private def hndlAjaxErr(dispatcher: Dispatcher, ajaxEx: AjaxException)
  : Unit = {
    val xhr = ajaxEx.xhr
    System.err.println (
      s"AJAX Error: ${xhr.statusText} (${xhr.status})\n  ${xhr.response}")

    closeModals()
    dispatcher dispatch PushNotificationAction(
      AppNotifications.ajaxError(ajaxEx))
  }

  /**
   * Handles Service Response Errors.
   *
   * @param dispatcher app dispatcher
   * @param resp       service error response
   */
  private def hndlSvcErr(dispatcher: Dispatcher, resp: dao.ServiceErrResp)
  : Unit = {
    System.err.println(
      s"Service Error: ${resp.sText} (${resp.status})\n  ${resp.body}")

    closeModals()
    dispatcher dispatch PushNotificationAction(
      AppNotifications.svcError(resp)
    )
  }

  /**
   * Handles Invalid PKI Error
   *
   * @param dispatcher app dispatcher
   * @param resp       service error response
   */
  private def hndlInvalidPki(dispatcher: Dispatcher, resp: dao.ServiceErrResp)
  : Unit = {
    System.err.println(
      s"PKI Error: ${resp.sText} (${resp.status})\n  ${resp.body}")

    closeModals()
    dispatcher dispatch PushNotificationAction(
      AppNotifications.invalidPki(resp)
    )
  }

  private def hndlBadResp(dispatcher: Dispatcher, resp: dao.ServiceErrResp)
  : Unit = {
    System.err.println(
      s"Bad Response: ${resp.sText} (${resp.status}) \n ${resp.body}")

    closeModals()
    dispatcher dispatch PushNotificationAction(
      AppNotifications.badResp(resp)
    )
  }

  /** Closes all modals */
  private def closeModals(): Unit = {
    appState.mainNavOpen_?.value = false
    appState.authState.value.fold (
      unauthState => {
        unauthState.loginOpen_?.value = false
        unauthState.signupOpen_?.value = false
      },
      authState => {
        authState.rerunModalOpen_?.value = false
        authState.geditModalOpen_?.value = false
      },
    )
  }
}

object AppNotifications {
  import cmdaa.ui.utils.intellijBindingHelper

  /** Helper for making AjaxError Notifications */
  def ajaxError(ajaxEx: AjaxException): Notification =
    Notification(NotificationType.Warning, "Ajax Error!", ajaxErrorBody(ajaxEx))

  /** Helper for making ServiceError Notifications */
  def svcError(resp: ServiceErrResp): Notification =
    Notification(NotificationType.Warning, "Service Error", svcErrBody(resp))

  /** Helper for making InvalidPKI Notifications */
  def invalidPki(resp: ServiceErrResp): Notification =
    Notification(NotificationType.Error, "Invalid PKI", invalidPkiBody(resp))

  /** Helper for making BadResponse Notifications */
  def badResp(resp: ServiceErrResp): Notification =
    Notification(NotificationType.Error, "Bad Response", badRespBody(resp))

  @html
  private def ajaxErrorBody(ajaxEx: AjaxException)
  : NodeBinding[Node] =
    <span><strong>Unexpected AJAX Error:
      { s"${ajaxEx.xhr.statusText} (${ajaxEx.xhr.status})" }</strong><br/>
      <small>See console for response body.</small>
    </span>

  @html
  private def svcErrBody(resp: dao.ServiceErrResp): NodeBinding[Node] =
    <span><strong>Service Error:
      { s"${resp.sText} (${resp.status})" }</strong><br/>
      <small>See console for response body.</small>
    </span>

  @html
  private def invalidPkiBody(resp: dao.ServiceErrResp): NodeBinding[Node] =
    resp.body map { j =>
      val s_dn = (j \ "s_dn").asOpt[String]
      val i_dn = (j \ "i_dn").asOpt[String]
      val ver  = (j \ "ver" ).asOpt[String]

      <span><strong>Invalid PKI Certificate</strong><br/>
        <small><strong>Subject DN:</strong>  {s_dn.getOrElse("N/A")}</small><br/>
        <small><strong>Issuer DN:</strong>   {i_dn.getOrElse("N/A")}</small><br/>
        <small><strong>Verifaction:</strong> {ver.getOrElse("N/A")}</small>
      </span>
    } getOrElse {
      <span><strong>Invalid PKI Certificate</strong><br/>
        <small><strong>Unexpected Response! See Console</strong></small>
      </span>
    }

  @html
  private def badRespBody(resp: dao.ServiceErrResp): NodeBinding[Node] =
    <span><strong>Unexpected Service Response:
      { s"${resp.sText} (${resp.status})" }</strong><br/>
      <small>See console for response body.</small>
    </span>
}
