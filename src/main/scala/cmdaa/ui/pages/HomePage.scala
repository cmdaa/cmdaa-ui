/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.pages

import cmdaa.ui.AppStateRO
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.forms.handleInput
import cmdaa.ui.router.Route
import cmdaa.ui.state.AuthenticatedActions.{EditGrok, EditDaemonSet, RerunCmdaaLog}
import cmdaa.ui.state.FileManagerActions._
import cmdaa.ui.state.LogUploaderActions.{ChooseFile, ClearFile, UploadFile}
import cmdaa.ui.state.RunManagerActions._
import cmdaa.ui.state.{FileManagerStateRO, LogUploaderStateRO, RunManagerStateRO}
import cmdaa.ui.utils._
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding._
import io.cmdaa.gs.api.{CmdaaLog, GrokRun, RunStatus}
import org.lrng.binding.html
import org.lrng.binding.html._
import org.scalajs.dom.raw._
import org.scalajs.dom.{Event, document, window}

import scala.scalajs.js

object HomePage extends Route[AppStateRO] {

  @html
  override def component(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val authState = Binding {
      // If this Page is in use, then we should be guaranteed to be in an
      // authenticated state.
      appState.authState.bind.toOption.get
    }

    val fileMngrState = Binding { authState.bind.fileMngrState }
    val runMngrState  = Binding { authState.bind.runMngrState }
    val has_files     = Binding { fileMngrState.bind.cmdaaLogs.nonEmpty.bind }

    val run_selected_? = Binding {
      runMngrState.bind.grokRun.bind.exists(_.groks.nonEmpty)
    }

    <div>
      <section class="section">
        <div class="container">
          <div class="columns">
            <div class="column is-three-fifths">
              <h1 class="title">CMDAA - MVP1</h1>
              <h2 class="subtitle">Welcome to CMDAA - MVP1, GrokMaker</h2>
              <p>To demonstrate our mighty Grok Making powers, we've created
                this humble MVP UI project. All you need to do is upload a log
                file using the dialog on the right.</p>
              <p style="margin-top: 10px">Just wait 'til you see what we can
                do!</p>
            </div>
            <div class="column is-two-fifths">
              <article class="tile is-parent notification is-primary is-vertical">
                <p class="title">Log Uploader</p>
                {
                  if (!has_files.bind)
                    <p class="subtitle">Don't be shy now.</p>
                  else
                    <!-- Subject has demonstrated lack of shyness through
                         previous file uploads -->
                }
                <div class="tile is-child is-vertical">
                  { fileUploader(authState.bind.uploadState).bind }
                  <br />
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>
      {
        if (has_files.bind)
          <section>
            <div class="container">
              <div class="columns">
                <div class="column is-three-fifths">
                  {
                    if (run_selected_?.bind) {
                      runManager(runMngrState.bind,
                        fileMngrState.bind.selCmdaaLog)
                    } else {
                      <!-- No run selected -->
                    }}
                </div>
                <div class="column is-two-fifths">
                  { fileManager(fileMngrState.bind) }
                </div>
              </div>
            </div>
          </section>
        else <!-- No files -->
      }
    </div>
  }

  /**
   * HomePage component for managing the user's uploaded files in S3. Viewing
   * them, deleting them, and pulling up their "runs".
   *
   * @param fileMngrState the State of the FileManager
   * @param dispatcher    the Action Dispatcher
   *
   * @return the HomePage/FileManager Binding Component
   */
  @html
  def fileManager(fileMngrState: FileManagerStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val logList = fileMngrState.cmdaaLogs

    <nav class="panel">
      <p class="panel-heading">Uploads</p>
      {
        if (logList.length.bind > 5)
          <div class="panel-block">
            <p class="control has-icons-left">
              <input class="input is-small" type="text" placeholder="filter"
                value={ fileMngrState.filter.bind }
                oninput={ handleInput(FilterChanged.apply)(_) }
                data:maxlength="25"
              />
              <span class="icon is-small is-left">
                <i class="fas fa-search" data:aria-hidden="true"></i>
              </span>
            </p>
          </div>
        else
          <!-- Not enough files to filter -->
      }
      <div id="fileContent">{
        logList
          .withFilter { _
            .pathData.logName.toLowerCase
            .contains(fileMngrState.filter.bind.toLowerCase)
          }
          .flatMapBinding(panelFileItem(fileMngrState))
      }</div>
      {
        if (logList.length.bind > 5)
          <div class="panel-block">
            <button
              class="button is-link is-outlined is-fullwidth"
              onclick={ _: Event => dispatcher dispatch FilterCleared }
            >Reset Filters</button>
          </div>
        else
          <!-- Not enough files to filter -->
      }
    </nav>
  }

  /**
   * HomePage component for viewing/managing Grok Runs.
   *
   * @param runMngrState the state of the RunManager component
   * @param dispatcher   the Action Dispatcher
   *
   * @return the HomePage/GrokRunManager component
   */
  @html
  private def runManager (
    runMngrState:   RunManagerStateRO,
    bo_selCmdaaLog: Binding[Option[CmdaaLog]]
  )(implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val groks = Binding {
      Constants(runMngrState.grokRun.bind.toList.flatMap(_.groks).zipWithIndex: _*)
    }

    <div id="runManager" class="card">
      <header class="card-header">
        <p class="card-header-title">Grok Run</p>
      </header>
      <div class="card-content">
        <div id="runContent" class="content">
          <table class="table is-size-7 is-striped is-narrow is-fullwidth is-hoverable">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Grok</th>
                <th>Logs</th>
              </tr>
            </thead>
            <tbody>{
              groks.bind.map { case (grok, idx) =>
                <tr>
                  <td>
                    <button
                      class="icon-btn"
                      onclick={ e: Event =>
                        e.stopPropagation()

                        for {
                          log <- bo_selCmdaaLog.asInstanceOf[Var[Option[CmdaaLog]]].value
                          run <- runMngrState.grokRun.asInstanceOf[Var[Option[GrokRun]]].value
                        } {
                          dispatcher dispatch EditGrok(log, run, idx, grok)
                        }
                      }
                    >
                      <span class="panel-icon"><i class="fas fa-edit"></i></span>
                    </button>
                  </td>
                  <td><small>{ grok.grok }</small></td>
                  <td><small>{ grok.logs.length.toString }</small></td>
                </tr>
              }
            }</tbody>
          </table>
        </div>
      </div>
      <footer class="card-footer">
        <a
          href={ runMngrState.objUrl.bind.getOrElse("") }
          class="card-footer-item"
          data:download="groks.json"
        >
          <i class="fas fa-download"></i>&nbsp;Download
        </a>
        { fluentDropdown(runMngrState).bind }
        { daemonDropdown(runMngrState, bo_selCmdaaLog).bind }
      </footer>
    </div>
  }

  private val fluentDdOpen_? = Var(false)
  private val daemonOpen_?   = Var(false)

  @html
  private def fluentDropdown(runMngrState: RunManagerStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val fluentDdClass = Binding {
      if (fluentDdOpen_?.bind) "is-active" else ""
    }

    val offClickFn: Var[Option[js.Function1[Event, _]]] = Var(None)
    def fluentDropdownOffClick(e: Event)
    : js.Function1[Event, _] = { (e2: Event) =>
      if (e != e2) {
        fluentDdOpen_?.value = false
        offClickFn.value.map { fn =>
          document.removeEventListener("click", fn)
        }
      }
    }

    <div class={s"dropdown is-up card-footer-item ${fluentDdClass.bind}"}>
      <div class="dropdown-trigger is-fullwidth">
        <button class="button is-outlined is-fullwidth is-align-content-baseline is-justify-content-space-between"
          data:aria-haspopup="true"
          data:aria-controls="dropdown-menu"
          onclick={ e: Event =>
            e.preventDefault()

            if (fluentDdOpen_?.value) {
              fluentDdOpen_?.value = false
            } else {
              fluentDdOpen_?.value = true
              val fn = fluentDropdownOffClick(e)
              offClickFn.value = Some(fn)
              document.addEventListener("click", fn)
            }
          }
        >
          <span class="icon is-small">
            <i class="fas fa-dove" data:aria-hidden="true"></i>
          </span>
          <span>FluentD</span>
          <span class="icon is-small">
            <i class="fas fa-angle-up" data:aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div id="fluent-dropdown-menu" class="dropdown-menu" data:role="menu">
        <div class="dropdown-content">
          {
            if (runMngrState.grokRun.bind.exists(_.associatedEsInstance.isEmpty)) {
              <a href="#" class="dropdown-item"
                 onclick={ e: Event =>
                   e.preventDefault()
                   dispatcher dispatch GenFluentd
                 }
              >
                <i class="fas fa-cog"></i>
                &nbsp; Start EFK
              </a>
            } else {
              <!-- ES Instance Created -->
            }
          }

          <a href="#" class="dropdown-item"
             onclick={ e: Event =>
               e.preventDefault()

               dispatcher dispatch DlFluentRequested (
                 document.getElementById("fluentDlLink")
                  .asInstanceOf[HTMLAnchorElement]
               )
             }
          >
            <i class="fas fa-download"></i>
            &nbsp; FluentD Config
          </a>

          {
            if (runMngrState.grokRun.bind.exists(_.associatedEsInstance.nonEmpty)) {
              <a href="#" class="dropdown-item"
                 onclick={ e: Event =>
                   e.preventDefault()
                   runMngrState.grokRun
                     .asInstanceOf[Var[Option[GrokRun]]].value
                     .flatMap(_.associatedEsInstance)
                     .foreach { kibanaHost =>
                       window.open(s"https://$kibanaHost", "_blank")
                     }
                 }
              >
                <i class="fas fa-eye"></i>
                &nbsp; View FluentD Results
              </a>
              <hr class="dropdown-divider" />
              <a href="#" class="dropdown-item"
                 onclick={ e: Event =>
                   e.preventDefault()
                   dispatcher dispatch DelEsInstance
                 }
              >
                <i class="fas fa-trash" style="color: red"></i>
                &nbsp; Delete EFK
              </a>
            } else {
              Constants.empty[Node]
            }
          }
        </div>
      </div>
      <a id="fluentDlLink" style="visibility: hidden">link</a>
    </div>
  }

  @html
  private def daemonDropdown (
    runMngrState:   RunManagerStateRO,
    bo_selCmdaaLog: Binding[Option[CmdaaLog]]
  )(implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val daemonClass = Binding {
      if (daemonOpen_?.bind) "is-active" else ""
    }

    val offClickFn: Var[Option[js.Function1[Event, _]]] = Var(None)
    def daemonDropdownOffClick(e: Event)
    : js.Function1[Event, _] = { (e2: Event) =>
      if (e != e2) {
        daemonOpen_?.value = false
        offClickFn.value.map { fn =>
          document.removeEventListener("click", fn)
        }
      }
    }

    val isConfigured: Binding[Boolean] = Binding {
      bo_selCmdaaLog.bind.exists(_.dsConfig.exists { dsConfig =>
        dsConfig.daemonPrefix.exists(_.nonEmpty) &&
          dsConfig.nodeLbls.nonEmpty && dsConfig.paths.nonEmpty
      })
    }
    val isDeployed: Binding[Boolean] = Binding {
      runMngrState.grokRun.bind.exists(_.dsDeployed)
    }

    <div class={s"dropdown is-up card-footer-item ${daemonClass.bind}"}>
      <div class="dropdown-trigger is-fullwidth">
        <button
          class="button is-outlined is-fullwidth is-align-content-baseline is-justify-content-space-between"
          data:aria-haspopup="true"
          data:aria-controls="dropdown-menu"
          onclick={ e: Event =>
            e.preventDefault()

            if (daemonOpen_?.value) {
              daemonOpen_?.value = false
            } else {
              daemonOpen_?.value = true
              val fn = daemonDropdownOffClick(e)
              offClickFn.value = Some(fn)
              document.addEventListener("click", fn)
            }
          }
        >
          <span class="icon is-small">
            <i class="fas fa-server" data:aria-hidden="true"></i>
          </span>
          <span>DaemonSet</span>
          <span class="icon is-small">
            <i class="fas fa-angle-up" data:aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div id="daemon-dropdown-menu" class="dropdown-menu" data:role="menu">
        <div class="dropdown-content">
          <a href="#" class="dropdown-item"
            onclick={ e: Event =>
              e.preventDefault()
              dispatcher dispatch EditDaemonSet
            }
          >
            <i class="fas fa-cog"></i>
            &nbsp; Configure...
          </a>
          {
            if (isDeployed.bind) {
              <a
                href="#"
                class="dropdown-item"
                onclick={ e: Event =>
                  e.preventDefault()
                  dispatcher dispatch StopDaemonSet
                }
              >
                <i class="fas fa-stop-circle"></i>
                &nbsp; Stop DaemonSet
              </a>
            } else if (isConfigured.bind) {
              <a
                href="#"
                class="dropdown-item"
                onclick={ e: Event =>
                  e.preventDefault()
                  dispatcher dispatch DeployDaemonSet
                }
              >
                <i class="fas fa-cog"></i>
                &nbsp; Launch DaemonSet
              </a>
            } else {
              <!-- DaemonSet is neither launched nor configured -->
            }
          }
        </div>
      </div>
    </div>
  }

  /**
   * Component for displaying panel items in the HomePage/fileManager
   * component.
   *
   * @param fileMngrState the state of the HomePage/fileManager component
   * @param log           the "log" being displayed
   * @param dispatcher    the Action Dispatcher
   *
   * @return a panelFileItem component for the given file
   */
  @html
  private def panelFileItem(fileMngrState: FileManagerStateRO)(log: CmdaaLog)
    (implicit dispatcher: Dispatcher)
  : Binding[NodeBindingSeq[Node]] = {
    val logActive_? = Binding {
      fileMngrState.selCmdaaLog.bind.exists(_.id contains log.id)
    }
    val logClass = Binding {
      if (logActive_?.bind) "is-active" else ""
    }
    val logIcon = Binding {
      if (logActive_?.bind) "fa-folder-open" else "fa-folder"
    }

    Binding {
      <a class={ s"panel-block ${logClass.bind}" }
        onclick={ e: Event =>
          e.preventDefault()
          dispatcher dispatch LogSelected(log)
        }
      >
        <span class="panel-icon">
          <i class={s"fas ${logIcon.bind}"} data:aria-hidden="true"></i>
        </span>
        <span>{ log.pathData.logName }</span>
        <span class="date">
          { date2display(new js.Date(log.pathData.lastModified.toDouble)) }
          <button class="icon-btn"
            title={ s"Run LL Workflow for CmdaaLog: ${log.id}"}
            onclick={ e: Event =>
              e.stopPropagation()
              dispatcher dispatch RerunCmdaaLog(log)
            }
          >
            <span class="panel-icon"><i class="fas fa-redo-alt"></i></span>
          </button>
          <button class="delete"
            title={ s"Delete CmdaaLog: ${log.id}" }
            onclick={ e:Event =>
              e.stopPropagation()
              dispatcher dispatch DeleteLog(log)
            }
          ></button>
        </span>
      </a>
      <div>{
        if (logActive_?.bind) {
          val runs =
            if (log.runs.isEmpty)
              List(GrokRun(
                "", log.meta.created,
                RunStatus.SubmittingWorkflowStatus,
                0.06f, 0.9f,
                associatedEsInstance = None,
                dsDeployed = false,
                groks = Nil
              ))
            else log.runs

          Constants(runs: _*) map { grokRun =>
            val runActive_? = Binding {
              fileMngrState.selGrokRunId.bind.contains(grokRun.id)
            }
            val runClass = Binding {
              if (runActive_?.bind) "is-active" else ""
            }

            val runIcon = grokRun.status match {
              case RunStatus(true,  false, false, _) => "fa-spin"
              case RunStatus(false, true,  false, _) => "has-text-success"
              case RunStatus(false, false, true,  _) => "has-text-danger"
              case RunStatus(false, false, false, _) => ""
              case rs: RunStatus =>
                println("RunStatus in unknown state: " + rs)
                "has-text-warning"
            }

            <a
              class={s"panel-block ${runClass.bind}"}
              style="padding-left: 20px;"
              onclick={ event: Event =>
                event.preventDefault()
                dispatcher dispatch GrokRunSelected(grokRun)
              }
            >
              <span class="panel-icon">
                <i class={s"fas fa-cog $runIcon"} data:aria-hidden="true"></i>
              </span>
              <span class="run-message">{ grokRun.status.message }</span>
              <span class="date">
                { date2display(new js.Date(strDt2millis(grokRun.submitted))) }
                &nbsp;
                <button class="delete" onclick={ e:Event =>
                  e.stopPropagation()
                  dispatcher dispatch DeleteRun(log, grokRun)
                }></button>
              </span>
            </a>.bind
          }
        } else {
          Constants.empty[Node]
        }
      }</div>
    }
  }

  /**
   * HomePage component for allowing users to upload log files into S3 for
   * processing.
   *
   * @param logUploaderState the current State of the Uploader Component
   * @param dispatcher  the Action Dispatcher
   *
   * @return the HomePage/fileUploader Binding Component
   */
  @html
  private def fileUploader(logUploaderState: LogUploaderStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val o_file = logUploaderState.selectedFile
    val o_upld = logUploaderState.uploadState

    val hasFileClass = Binding {
      if (o_file.bind.isDefined) "has-name" else ""
    }

    val b_showControls_? = Binding {
      o_file.bind.isDefined && o_upld.bind.isEmpty
    }
    val bo_disableFI = Binding {
      if (o_upld.bind.nonEmpty) Some("disabled") else None
    }

    <form id="fileUploadForm"
      onsubmit={ e: Event =>
        e.preventDefault()
        dispatcher dispatch UploadFile
      }
    >
      <div class="field">
        <div class={s"file is-boxed is-centered $hasFileClass"}>
          <div class="control">
            <label class="file-label">
              <input
                id="fileUploadInput"
                class="file-input" type="file" name="logs"
                onchange={ handleFileChoice(_) }
                disabled={ bo_disableFI.bind }
              />
              <span class="file-cta">
                <span class="file-icon"><i class="fas fa-upload"></i></span>
                <span class="file-label">Choose a log file...</span>
              </span>
              {
                o_file.bind match {
                  case Some(file) =>
                    <span class="file-name">{ file.name }</span>.bind
                  case _ =>
                    <!-- filename -->.bind
                }
              }
            </label>
          </div>
        </div>
      </div>
      {
        if (b_showControls_?.bind) {
          <div class="field is-grouped is-grouped-centered">
            <div class="control">
              <button type="submit" class="button is-link">Upload</button>
            </div>
            <div class="control">
              <button
              class="button"
              onclick={ _: Event => dispatcher dispatch ClearFile }
              >Clear</button>
            </div>
          </div>.bind
        } else {
          <!-- form controls -->.bind
        }
      } {
        o_upld.bind match {
          case Some(upldState) =>
            <progress
              class="progress is-info is-large"
              value={upldState.loaded}
              max={upldState.total}
            >{(100.0*upldState.loaded / upldState.total).toString}%</progress>.bind
          case _ =>
            <!-- upload state -->.bind
        }
      }
    </form>
  }

  /**
   * DOM Event handler for an Input of type "file". On change, we dispatch an
   * event, notifying the Application that a file has been selected.
   *
   * @param event      the Input Change Event
   * @param dispatcher ActionDispatcher
   */
  private def handleFileChoice(event: Event)
    (implicit dispatcher: Dispatcher)
  : Unit = {
    val fileList = event.currentTarget
      .asInstanceOf[HTMLInputElement]
      .files

    // The way the form selector works... can this ever be 0?
    if (fileList.length == 0)
      dispatcher dispatch ClearFile
    else
      dispatcher dispatch ChooseFile(fileList(0))
  }
}
