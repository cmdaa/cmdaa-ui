/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.pages

import cmdaa.ui.AppStateRO
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.router.Route
import cmdaa.ui.utils.intellijBindingHelper
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.raw.Node

object UnauthHomePage extends Route[AppStateRO] {

  @html
  override def component(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <div>
      <section class="section">
        <div class="container">
          <div class="columns">
            <div class="column is-three-fifths">
              <h1 class="title">CMDAA - MVP1</h1>
              <h2 class="subtitle">Welcome to CMDAA - MVP1, GrokMaker</h2>
              <p>To demonstrate our mighty Grok Making powers, we've created
                this humble MVP UI project. All you need to do is upload a log
                file using the dialog on the right.</p>
              <p style="margin-top: 10px">
                Just wait 'til you see what we can do!
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
}
