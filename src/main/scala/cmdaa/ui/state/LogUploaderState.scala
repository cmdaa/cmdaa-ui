/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.dao.{GrokstoreDao, LogDao}
import cmdaa.ui.dispatcher._
import cmdaa.ui.state.AuthenticatedActions.StartWorkflow
import cmdaa.ui.utils.FileUploadCallbacks
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.req.NewCmdaaLog
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.raw.{File, HTMLInputElement}
import org.scalajs.dom.{ErrorEvent, ProgressEvent, document}

import scala.scalajs.js
import scala.util.Try

case class UploadState(loaded: Double, total: Double)

/**
 * The mutable state for the LogUploader Component.
 *
 * @param selectedFile the selected file (from the client's computer) to
 *                     upload
 */
case class LogUploaderState (
  selectedFile: Var[Option[File]]        = Var(None),
  uploadState:  Var[Option[UploadState]] = Var(None)
) extends StateModel[LogUploaderStateRO] {
  override def toRO: LogUploaderStateRO =
    LogUploaderStateRO(selectedFile, uploadState)
}

/** The "immutable" state for the LogUploader Component */
case class LogUploaderStateRO (
  selectedFile: Binding[Option[File]],
  uploadState:  Binding[Option[UploadState]]
) extends StateModelRO

/** Actions for the LogUploader Component. */
object LogUploaderActions {
  /** Fired when a file has been chosen in the Log Uploader */
  case class ChooseFile(file: File) extends StateAction

  /** Fired when the "clear" button is pressed in the Log Uploader. */
  case object ClearFile extends StateAction

  /** Fired when the "upload" button is pressedin the Upload File. */
  case object UploadFile extends StateAction

  /** File when a new CmdaaLog is to be inserted into GrokStore. */
  case class CreateCmdaaLog(newCmdaaLog: NewCmdaaLog) extends StateAction

  /** File upload initiated. */
  case class UploadBegan(loaded: Double, total: Double)
  extends StateAction

  /** File upload progressed. */
  case class UploadProgress(loaded: Double, total: Double)
  extends StateAction

  /** Event fired when the upload has successfully finished. */
  case object UploadLoaded extends StateAction

  /** Event fired when the upload was aborted. */
  case object UploadAborted extends StateAction

  /** Event fired when the upload errors out. */
  case class UploadError(msg: String) extends StateAction

  /** Event fired when the upload timed out. */
  case object UploadTimeout extends StateAction
}

/**
 * ActionHandler for the LogUploader Component.
 *
 * @param logUploaderState mutable LogUploaderState
 * @param sessToken   user's Authenication Token for interacting with services
 */
class LogUploaderActionHandler (
  private val logUploaderState: LogUploaderState,
  private val sessToken: Var[SessionToken]
) extends ActionHandler {
  import cmdaa.ui.state.LogUploaderActions._

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
  import scala.util.{Failure, Success}

  override protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case CreateCmdaaLog(newCmdaaLog) =>
      hndlCreateCmdaaLog(dispatcher, newCmdaaLog)

    case ChooseFile(file) => hndlChooseFile(file)
    case ClearFile  => hndlClearFile()
    case UploadFile => hndlUploadFile(dispatcher)

    case UploadBegan(loaded: Double, total: Double) =>
      logUploaderState.uploadState.value = Some(UploadState(loaded, total))
    case UploadProgress(loaded: Double, total: Double) =>
      logUploaderState.uploadState.value = Some(UploadState(loaded, total))
    case UploadLoaded =>
      println("UploadLoaded")
    case UploadAborted =>
      println("UploadAborted")
    case UploadError(msg) =>
      println("UploadError: " + msg)
    case UploadTimeout =>
      logUploaderState.uploadState.value = None

    case _ =>
  }

  /**
   * Handles the ChooseFile action. Occurs when the user has chosen a log file
   * to upload.
   *
   * @param file the chosen file to upload
   */
  private def hndlChooseFile(file: File): Unit = {
    logUploaderState.selectedFile.value = Some(file)
  }

  /**
   * Handles the ClearFile action. Occurs whenever the chosen file needs to be
   * cleared.
   */
  private def hndlClearFile(): Unit = {
    logUploaderState.selectedFile.value = None
    logUploaderState.uploadState.value  = None

    // File Inputs seems to be special and dislike when their values are
    // changed through bindings. Therefore...
    Option(document.getElementById("fileUploadInput"))
      .foreach { case input: HTMLInputElement => input.value = "" }
  }

  /**
   * Handles the UploadFile action, when the user uploads the selected file.
   *
   * @param dispatcher application action dispatcher
   */
  private def hndlUploadFile(dispatcher: Dispatcher): Unit = {
    logUploaderState.selectedFile.value.fold {
      println("Can't upload. No file selected.")
    } { file =>
      if (file.size > 0) {
        val lmDate = lastModified(file)
        println(s"Uploading file: '${file.name}_$lmDate'.")
        uploadFile(dispatcher, file, lmDate)
      } else {
        println(s"Cowardly refusing to upload empty file: ${file.name}")
      }
    }
  }

  /**
   * Uses the DAOs to upload the file to S3.
   *
   * @param dispatcher application action dispatcher
   * @param file       the file to upload
   * @param lmDate     the lastModified timestamp of the file
   */
  private def uploadFile(dispatcher: Dispatcher, file: File, lmDate: Long)
  : Unit = {
    val callbacks = FileUploadCallbacks (
      onLoadStart = (a: js.Any) => {
        val action = Try(a.asInstanceOf[ProgressEvent]).toOption
          .map(pe => UploadBegan(pe.loaded, pe.total))
          .getOrElse(UploadBegan(0d, 100d))

        dispatcher dispatch action
      },
      onProgress = (pe: ProgressEvent) => {
        dispatcher dispatch UploadProgress(pe.loaded, pe.total)
      },
      onAbort = (_: js.Any) => {
        dispatcher dispatch UploadAborted
      },
      onLoad = (_: js.Any) => {
        dispatcher dispatch UploadLoaded
      },
      onError = (ee: ErrorEvent) => {
        dispatcher dispatch UploadError(ee.message)
      },
      onTimeOut = (_: js.Any) => {
        dispatcher dispatch UploadTimeout
      }
    )

    LogDao
      .upload(sessToken.value, file, lmDate, callbacks)
      .onComplete {
        case Success(v) =>
          println(s"File: '${v.path}' stored in bucket: '${v.path}'")
          dispatcher dispatch ClearFile
          dispatcher dispatch CreateCmdaaLog (
            NewCmdaaLog(v.bucket, v.path, file.name,
              llThreshold = 0.06, cosThreshold = 0.9)
          )
        case Failure(ex: AjaxException) =>
          println("Error submitting Log File. " + ex.getMessage)
        case Failure(ex) =>
          println("Error: " + ex.getMessage)
      }
  }

  /**
   * Handles the CreateCmdaaLog Action.
   *
   * @param newCmdaaLog data for creating a new CmdaaLog
   */
  def hndlCreateCmdaaLog(dispatcher: Dispatcher, newCmdaaLog: NewCmdaaLog)
  : Unit = {
    GrokstoreDao
      .insertCmdaaLog(sessToken.value, newCmdaaLog)
      .onComplete {
        case Success(cmdaaLog) =>
          val run = cmdaaLog.runs.head
          println(s"Inserted CmdaaLog: ${cmdaaLog.id} / ${run.id}")
          dispatcher dispatch StartWorkflow(cmdaaLog, run)

        case Failure(ex: AjaxException) =>
          println(s"AJAX Error creating new log in Grokstore. ${ex.getMessage}")
        case Failure(ex) =>
          println(s"Error creating new log in Grokstore. ${ex.getMessage}")
      }
  }

  /**
   * Tries to determine the lastModified time in millis for the Uploaded file.
   * If the browser doesn't support it, it defaults to Now.
   *
   * @param file the file to extract the LastModifiedDate from
   */
  private def lastModified(file: File): Long = {
    //noinspection ScalaDeprecation
    val lmd = file.lastModifiedDate

    if (lmd != null && lmd.isInstanceOf[js.Date])
      lmd.asInstanceOf[js.Date].valueOf().toLong
    else
      System.currentTimeMillis()
  }
}
