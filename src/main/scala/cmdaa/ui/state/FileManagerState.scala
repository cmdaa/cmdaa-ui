/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.AppActions.LogoutAction
import cmdaa.ui.dao.{GrokstoreDao, HttpError, LogDao, UnauthenticatedError}
import cmdaa.ui.dispatcher._
import cmdaa.ui.msg.LogMsgs.LogData
import cmdaa.ui.state.AuthenticatedActions.CmdaaLogsChanged
import cmdaa.ui.state.RunManagerActions.GrokRunLoaded
import cmdaa.ui.utils.strDt2millis
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.{BindingSeq, Var, Vars}
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.{CmdaaLog, GrokRun}
import org.scalajs.dom.Blob
import org.scalajs.dom.raw.{BlobPropertyBag, HTMLAnchorElement, URL}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration
import scala.scalajs.js
import scala.scalajs.js.timers.SetTimeoutHandle

/**
 * The mutable state for the FileManager Component state.
 *
 * @param cmdaaLogs    list CMDAA Logs
 * @param filter       filter for finding files
 * @param selCmdaaLog  the selected log file
 * @param selGrokRunId the selected Grok run
 *
 * @param scheduledQuery tracks scheduled query actions so there should only be
 *                       one at any given time. Also makes the Scheduled Query
 *                       cancelable, which may be desired on page change.
 */
case class FileManagerState (
  cmdaaLogs:    Vars[CmdaaLog]        = Vars.empty,
  selCmdaaLog:  Var[Option[CmdaaLog]] = Var(None),
  selGrokRunId: Var[Option[String]]   = Var(None),
  filter:       Var[String]           = Var(""),

  scheduledQuery: Var[Option[SetTimeoutHandle]] = Var(None)
) extends StateModel[FileManagerStateRO] {
  val cmdaaLogMap: Binding[Map[String, CmdaaLog]] = Binding {
    cmdaaLogs.asInstanceOf[BindingSeq[CmdaaLog]].all
      .bind.map(l => l.id -> l).toMap
  }

  override def toRO: FileManagerStateRO =
    FileManagerStateRO(cmdaaLogs, selCmdaaLog, selGrokRunId, filter)
}

/**
 * The immutable representation of the FileManager Component state.
 *
 * @param cmdaaLogs    list of CMDAA logs
 * @param selCmdaaLog  the selected log file
 * @param selGrokRunId the selected Grok run
 * @param filter       filter for finding files
 */
case class FileManagerStateRO (
  cmdaaLogs:    BindingSeq[CmdaaLog],
  selCmdaaLog:  Binding[Option[CmdaaLog]],
  selGrokRunId: Binding[Option[String]],
  filter:       Binding[String]
) extends StateModelRO

object FileManagerActions {
  /** Fired whenever the FileList should be updated. */
  case object UpdateFileList extends StateAction

  /** Fired when the Queried CmdaaLogs have been retrieved. */
  case class CmdaaLogsListed(cmdaaLogs: List[CmdaaLog]) extends StateAction

  /** Fired when a S3 File has been selected */
  case class FileSelected(file: LogData) extends StateAction

  /** Fired with a CmdaaLog has been selected. */
  case class LogSelected(log: CmdaaLog) extends StateAction

  /** Fired when a Run has been selected */
  case class GrokRunSelected(grokRun: GrokRun) extends StateAction

  /** Fired when the filter text has changed */
  case class FilterChanged(txt: String) extends StateAction

  /** Fired when the filter is cleared */
  case object FilterCleared extends StateAction

  case class DlFluentConfig(grokRun: GrokRun, anchor: HTMLAnchorElement)
  extends StateAction
}

/** ActionHandler for the FileManager component */
class FileManagerActionHandler (
  private val fileManagerState: FileManagerState,
  private val sessToken: Var[SessionToken]
) extends ActionHandler {
  import cmdaa.ui.state.FileManagerActions._
  import cmdaa.ui.state.RunManagerActions.ClearRun

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case UpdateFileList         => hndlUpdateFileList(dispatcher)
    case CmdaaLogsListed(logs)  => hndlLogsListed(dispatcher, logs)
    case LogSelected(log)       => hndlLogSelected(dispatcher, log)
    case GrokRunSelected(run)   => hndlRunSelected(dispatcher, run)
    case CmdaaLogsChanged(logs) => hndlLogsChanged(logs)

    case action: DlFluentConfig => hndlDlFluent(action)

    case FilterChanged(txt) => fileManagerState.filter.value = txt
    case FilterCleared      => fileManagerState.filter.value = ""
    case _ =>
  }

  private def hndlDlFluent(action: DlFluentConfig): Unit = {
    fileManagerState.selCmdaaLog.value.fold {
      println("No CmdaaLog Selected; How did we get here?!")
    } { cmdaaLog =>
      val fluentPath = cmdaaLog.path.substring(0, cmdaaLog.path.length-4) +
        s"/${action.grokRun.id}.conf"

      println(s"Fetching - $fluentPath")

      LogDao.fetchText(sessToken.value, fluentPath) foreach { result =>
        val url = URL.createObjectURL(new Blob (
          js.Array(result),
          BlobPropertyBag("text/plain")
        ))
        action.anchor.setAttribute("href", url)
        action.anchor.setAttribute("download", "fluentd.conf")
        action.anchor.click()
        URL.revokeObjectURL(url)
      }
    }
  }

  /**
   * Handles UpdateFileList actions by querying for the file list. Send an
   * updated File list to the dispatcher via the FileListed action.
   *
   * @param dispatcher dispatches actions/events
   */
  private def hndlUpdateFileList(dispatcher: Dispatcher): Unit = {
    // The scheduled task has executed. Remove it from the state so it can be
    // scheduled again.
    fileManagerState.scheduledQuery.value = None

    GrokstoreDao.listCmdaaLogs(sessToken.value)
      .onComplete(_.fold({
        case err: HttpError =>
          System.err.println(
            s"""Unhandled HttpError listing CmdaaLogs from Grokstore:
               |${err.status}/${err.sText} -
               |${err.rText}""".stripMargin.replaceAll("\n", "")
          )
          scheduleQuery(dispatcher)
        case UnauthenticatedError =>
          dispatcher dispatch LogoutAction
      }, { cmdaaLogs =>
        dispatcher dispatch CmdaaLogsListed(cmdaaLogs)
      }))
  }

  /**
   * Handles CmdaaLogsListed actions by scheduling the next UpdateFileList
   * action and managing the client's state.
   *
   * @param dispatcher dispatches actions/events
   * @param logs       the Cmdaa Logs fetched from GrokStore
   */
  private def hndlLogsListed(dispatcher: Dispatcher, logs: List[CmdaaLog])
  : Unit = {
    scheduleQuery(dispatcher)

    val cmdaaLogs = fileManagerState.cmdaaLogs.value
    if (cmdaaLogs.toList != logs) {
      cmdaaLogs.clear()
      cmdaaLogs ++= logs
      dispatcher dispatch CmdaaLogsChanged(logs)
    }
  }

  /**
   * Handles CmdaaLogsChanged action. If there's a selected CmdaaLog; look for
   * it by ID in the new log data; if it's there; update the selected CmdaaLog;
   * otherwise, unselect the CmdaaLog.
   *
   * @param newLogs the new log data to integrate
   */
  private def hndlLogsChanged(newLogs: List[CmdaaLog]): Unit = {
    fileManagerState.selCmdaaLog.value foreach { selected =>
      newLogs.find(_.id == selected.id).fold {
        fileManagerState.selCmdaaLog.value = None
      } { newLog =>
        fileManagerState.selCmdaaLog.value = Some(newLog)
      }
    }
  }

  /** Schedules the File/Log to happen in the future. */
  private def scheduleQuery(dispatcher: Dispatcher): Unit = {
    val scheduledQuery = fileManagerState.scheduledQuery

    if (scheduledQuery.value.isEmpty) {
      val toHndle = js.timers
        .setTimeout(FiniteDuration(10, TimeUnit.SECONDS)) {
          dispatcher dispatch UpdateFileList
        }
      scheduledQuery.value = Some(toHndle)
    }
  }

  /** Handles CMDAA Log Selection */
  private def hndlLogSelected(dispatcher: Dispatcher, log: CmdaaLog): Unit = {
    fileManagerState.selGrokRunId.value = None

    if (fileManagerState.selCmdaaLog.value.exists(_.id contains log.id)) {
      dispatcher dispatch ClearRun
      fileManagerState.selCmdaaLog.value = None
    } else {
      fileManagerState.selCmdaaLog.value = Some(log)
    }
  }

  /** Handle GrokRun selection */
  private def hndlRunSelected(dispatcher: Dispatcher, run: GrokRun): Unit = {
    if (fileManagerState.selGrokRunId.value contains run.id) {
      fileManagerState.selGrokRunId.value = None
      dispatcher dispatch ClearRun
    } else {
      fileManagerState.selGrokRunId.value = Some(run.id)

      val log = fileManagerState.selCmdaaLog.value.get
      dispatcher dispatch GrokRunLoaded(log, run)
    }
  }
}
