/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.dao.AuthDao
import cmdaa.ui.dispatcher._
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.PkiUserInfo
import io.cmdaa.auth.api.req.SignupReq
import org.scalajs.dom.document
import org.scalajs.dom.raw.HTMLInputElement

import scala.util.{Failure, Success}

/** The mutable state for the Signup Form */
case class SignupState (
  pkiEnabled: Binding[Boolean] = Var(false)
) extends StateModel[SignupStateRO] {
  /** The username for the new user being created */
  val username: Var[String] = Var("")

  /** Whether or not the username field should be readonly */
  val username_ro: Var[Boolean] = Var(false)

  /** Email address for the new user being created */
  val email: Var[String] = Var("")

  /** Whether or not the email field should be readonly */
  val email_ro: Var[Boolean] = Var(false)

  /** The firstName of the new user being created */
  val firstName: Var[String] = Var("")

  /** Whether or not the firstName field should be readonly */
  val firstName_ro: Var[Boolean] = Var(false)

  /** The lastName of the new user being created */
  val lastName: Var[String] = Var("")

  /** Whether or not the lastName field should be readonly */
  val lastName_ro: Var[Boolean] = Var(false)

  /** The password for the new user being created */
  val password: Var[String] = Var("")

  /** Verify Password value */
  val verifyPwd: Var[String] = Var("")

  override def toRO: SignupStateRO = SignupStateRO (
    pkiEnabled,
    username,  username_ro,
    email,     email_ro,
    firstName, firstName_ro,
    lastName,  lastName_ro,
    password,  verifyPwd
  )

  /** Clears the Signup Form */
  protected[state] def clear(): Unit = {
    username.value     = ""
    username_ro.value  = false
    email.value        = ""
    email_ro.value     = false
    firstName.value    = ""
    firstName_ro.value = false
    lastName.value     = ""
    lastName_ro.value  = false
    password.value     = ""
    verifyPwd.value    = ""
  }

  /** Converts Signup Form data to a NewUserMsg */
  def toMsg: SignupReq = {
    val pw = if (!pkiEnabled.asInstanceOf[Var[Boolean]].value)
      Some(password.value) else None

    SignupReq (
      username.value,
      email.value,
      pw,
      firstName.value,
      lastName.value
    )
  }
}

/**
 * The immutable state for the Signup Form
 *
 * @param pkiEnabled   whether or not auth is handled through PKI
 * @param username     username for the new user being created
 * @param username_ro  username readonly
 * @param email        email address for the new user being created
 * @param email_ro     email readonly
 * @param firstName    (optional) first name of the new user being created
 * @param firstName_ro firstName readonly
 * @param lastName     (optional) last name of the new user being created
 * @param lastName_ro  lastName readonly
 * @param password     (not for pki) password for the new user being created
 * @param verifyPwd    (not for pki) password verification for the new user
 *                       being created
 */
case class SignupStateRO (
  pkiEnabled:   Binding[Boolean],
  username:     Binding[String],
  username_ro:  Binding[Boolean],
  email:        Binding[String],
  email_ro:     Binding[Boolean],
  firstName:    Binding[String],
  firstName_ro: Binding[Boolean],
  lastName:     Binding[String],
  lastName_ro:  Binding[Boolean],
  password:     Binding[String],
  verifyPwd:    Binding[String]
) extends StateModelRO

/** Signup specific actions handled by the SignupActionHandler */
object SignupActions {
  /** Seeds the signup form with information from the Auth Service */
  case class SeedForm(userInfo: PkiUserInfo)
  extends StateAction

  /** Signup Form username changed event */
  case class SignupUsernameChanged(txt: String)
  extends StateAction

  /** Signup Form password changed event */
  case class SignupPasswordChanged(txt: String)
  extends StateAction

  /** Signup Form verifyPwd changed event */
  case class SignupVerifyPwdChanged(txt: String)
  extends StateAction

  /** Signup Form email changed event */
  case class SignupEmailChanged(txt: String)
  extends StateAction

  /** Signup Form firstName changed event */
  case class SignupFirstNameChanged(txt: String)
  extends StateAction

  /** Signup Form lastName changed event */
  case class SignupLastNameChanged(txt: String)
  extends StateAction

  /** Signup Form submission event */
  case object SignupFormSubmit
  extends StateAction

  /** Signup Form clear event */
  case object SignupFormClear
  extends StateAction
}

/**
 * ActionHandler for the Signup Form. Reacts to SignupForm Actions as well as
 * Application Actions regarding the Signup Modal.
 *
 * @param signupState the mutable SignupForm state
 */
class SignupActionHandler (
  private val signupState: SignupState
) extends ActionHandler {
  import SignupActions._

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case UnauthenticatedActions.SignupOpen(true) =>
      Option(document.getElementById("signupUsername"))
        .foreach { case input: HTMLInputElement => input.focus() }

    case UnauthenticatedActions.SignupOpen(false) => signupState.clear()
    case SignupFormSubmit => hndlSignupSubmit(dispatcher)
    case SignupFormClear  => signupState.clear()

    case SeedForm(userInfo) => hndlSeedForm(dispatcher, userInfo)

    //// Change Handlers ////
    case SignupUsernameChanged(txt)  => signupState.username.value  = txt
    case SignupPasswordChanged(txt)  => signupState.password.value  = txt
    case SignupVerifyPwdChanged(txt) => signupState.verifyPwd.value = txt
    case SignupEmailChanged(txt)     => signupState.email.value     = txt
    case SignupFirstNameChanged(txt) => signupState.firstName.value = txt
    case SignupLastNameChanged(txt)  => signupState.lastName.value  = txt

    case _ =>
  }

  /**
   * Seeds the signup form with information derived from the User Auth Service.
   * Fields with discovered information are readonly.
   *
   * @param dispatcher app dispatcher
   * @param userInfo   the discovered user info to seed the signup form with
   */
  private def hndlSeedForm(dispatcher: Dispatcher, userInfo: PkiUserInfo)
  : Unit = {
    userInfo.username foreach { v =>
      signupState.username.value  = v
      signupState.username_ro.value = true
    }
    userInfo.email foreach { v =>
      signupState.email.value = v
      signupState.email_ro.value = true
    }
    userInfo.firstName foreach { v =>
      signupState.firstName.value = v
      signupState.firstName_ro.value = true
    }
    userInfo.lastName foreach { v =>
      signupState.lastName.value = v
      signupState.lastName_ro.value = true
    }

    dispatcher dispatch UnauthenticatedActions.SignupOpen(true)
  }

  /**
   * Handles the SignupForm submit. Sends signup information to the auth
   * service.
   *
   * @param dispatcher app dispatcher
   */
  private def hndlSignupSubmit(dispatcher: Dispatcher): Unit =
    AuthDao
      .signup(signupState.toMsg)
      .onComplete {
        case Success(Some(createdUser)) =>
          dispatcher.dispatch (
            UnauthenticatedActions.UserCreated(createdUser.user) )
        case Success(None) =>
          println("TODO: Handle this better")
        case Failure(ex) =>
          println(s"Server Blew Up?? ${ex.getMessage}")
          println("TODO: Handle this better")
      }
}
