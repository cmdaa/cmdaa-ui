/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.AppActions
import cmdaa.ui.AppActions.LoginAction
import cmdaa.ui.dao.WorkflowDao
import cmdaa.ui.dispatcher._
import cmdaa.ui.forms.{EditGrokForm, DaemonConfigForm}
import cmdaa.ui.msg.WorkflowMsgs.LlWorkflowMsg
import cmdaa.ui.state.FileManagerActions.UpdateFileList
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.{CmdaaLog, Grok, GrokRun}
import org.scalajs.dom.ext.AjaxException

import scala.util.{Failure, Success}

/**
 * The mutable state for the Application's "Authenticated" state.
 *
 * @param sessToken the authentication token for identifying the user
 */
case class AuthenticatedState (
  sessToken: Var[SessionToken],

  rerunModalOpen_? : Var[Boolean] = Var(false),
  geditModalOpen_? : Var[Boolean] = Var(false),
  dsModalOpen_?    : Var[Boolean] = Var(false)
) extends StateModel[AuthenticatedStateRO] {
  val uploadState:   LogUploaderState = LogUploaderState()
  val fileMngrState: FileManagerState = FileManagerState()
  val runMngrState:  RunManagerState  = RunManagerState()

  val rerunFormState: RerunFormState =
    RerunFormState(fileMngrState.selCmdaaLog)

  val editGrokFormState: EditGrokForm.State =
    EditGrokForm.State(fileMngrState.selCmdaaLog)

  val dsFormState: DaemonConfigForm.State =
    DaemonConfigForm.State(fileMngrState.selCmdaaLog)

  override def toRO: AuthenticatedStateRO =
    AuthenticatedStateRO (
      sessToken,
      rerunModalOpen_?,
      geditModalOpen_?,
      dsModalOpen_?,
      rerunFormState.toRO,
      editGrokFormState.toRO,
      dsFormState.toRO,
      uploadState.toRO,
      fileMngrState.toRO,
      runMngrState.toRO
    )
}
object AuthenticatedState {
  def apply(sessToken: SessionToken): AuthenticatedState =
     AuthenticatedState(Var(sessToken))
}

/**
 * The immutable state for the Application's "Authenticated" state.
 *
 * @param sessToken  the authentication token for identifying the user
 */
case class AuthenticatedStateRO (
  sessToken:             Binding[SessionToken],
  rerunModalOpen_? :     Binding[Boolean],
  geditModalOpen_? :     Binding[Boolean],
  daemonSetModalOpen_? : Binding[Boolean],
  rerunFormState:        RerunFormStateRO,
  editGrokFormState:     EditGrokForm.StateRO,
  dsConfigFormState:     DaemonConfigForm.StateRO,
  uploadState:           LogUploaderStateRO,
  fileMngrState:         FileManagerStateRO,
  runMngrState:          RunManagerStateRO
) extends StateModelRO

/**
 * Application-wide Actions that are only available while the App is in an
 * "authenticated" state.
 */
object AuthenticatedActions {
  /** Action to initiate a workflow for a Log at the given S3Path */
  case class StartWorkflow(l: CmdaaLog, r: GrokRun)
  extends StateAction {
    def toMsg: LlWorkflowMsg = {
      LlWorkflowMsg (
        logId        = l.id,
        runId        = r.id,
        path         = l.path,
        llThreshold  = r.logLikelihood,
        cosThreshold = r.cosSimilarity,
        bucket       = l.bucket
      )
    }
  }

  /** Auth Action for opening/closing the RerunForm Modal */
  case class RerunModalOpen(value: Boolean)
  extends StateAction

  /** Auth Action for opening/closing the GrokEdit Modal */
  case class GeditModalOpen(value: Boolean)
  extends StateAction

  /** Auth Action for opening/closing the DaemonSet Config Modal */
  case class DsConfigOpen(value: Boolean)
  extends StateAction

  /** Auth Action for rerunning a CmdaaLog */
  case class RerunCmdaaLog(log: CmdaaLog)
  extends StateAction

  /** Auth Action for Editing a Grok. */
  case class EditGrok (
    log: CmdaaLog,
    grokRun: GrokRun,
    grokIdx: Int,
    grok: Grok
  ) extends StateAction

  /** Auth Action for Editing a DaemonSet Configuration */
  case object EditDaemonSet extends StateAction

  /** A tangible update has occured with CmdaaLogs. */
  case class CmdaaLogsChanged(logs: List[CmdaaLog])
  extends StateAction
}

/**
 * ActionHandler for Application-wide Actions that are only available while
 * the App is in an "authenticated" state.
 *
 * @param authState the mutable Authenticated State
 */
class AuthenticatedActionHandler (
  private val authState: AuthenticatedState
) extends ActionHandler {
  import cmdaa.ui.state.AuthenticatedActions._
  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override def subHandlers: List[ActionHandler] = List (
    new RerunFormActionHandler (
      authState.rerunFormState,
      authState.sessToken
    ),
    new LogUploaderActionHandler (
      authState.uploadState,
      authState.sessToken
    ),
    new FileManagerActionHandler (
      authState.fileMngrState,
      authState.sessToken
    ),
    new RunManagerActionHandler (
      authState.runMngrState,
      authState.sessToken
    ),
    new EditGrokForm.Handler (
      authState.editGrokFormState,
      authState.sessToken
    ),
    new DaemonConfigForm.Handler(
      authState.dsFormState,
      authState.sessToken
    )
  )

  override def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case RerunCmdaaLog(log) =>
      authState.rerunFormState.selCmdaaLog.value = Some(log)
      dispatcher dispatch RerunModalOpen(true)

    case _: EditGrok =>
      dispatcher dispatch GeditModalOpen(true)

    case EditDaemonSet =>
      dispatcher dispatch DsConfigOpen(true)

    case RerunModalOpen(newState) =>
      dispatcher dispatch AppActions.MainNavOpen(false)
      authState.rerunModalOpen_?.value = newState

    case GeditModalOpen(newState) =>
      dispatcher dispatch AppActions.MainNavOpen(false)
      authState.geditModalOpen_?.value = newState

    case DsConfigOpen(newState) =>
      dispatcher dispatch AppActions.MainNavOpen(false)
      authState.dsModalOpen_?.value = newState

    case LoginAction(_) =>
      dispatcher dispatch UpdateFileList

    case swAction: StartWorkflow =>
      submitWorkflow(swAction)

    case _ =>
  }

  /**
   * Submits a workflow for the given s3path
   *
   * @param swAction the StartWorkflow Action
   */
  private def submitWorkflow(swAction: StartWorkflow): Unit =
    WorkflowDao
      .runIngest(swAction.toMsg)
      .onComplete {
        case Success(Some(v)) =>
          println("Workflow.submitWorkflow Response - " + v)
        case Success(None) =>
          println("Success None?")
        case Failure(ex: AjaxException) =>
          println("Error in WorkflowDao.submitLlWorkflow - " + ex.getMessage)
        case Failure(ex) =>
          println("Error: " + ex.getMessage)
      }
}
