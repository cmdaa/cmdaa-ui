/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.AppActions.LogoutAction
import cmdaa.ui.dao._
import cmdaa.ui.dispatcher._
import cmdaa.ui.msg.LogMsgs.{GrokMessage, GrokOutputResp, LogMessage}
import cmdaa.ui.msg.WorkflowMsgs.{DeployDaemonSetMsg, StopDaemonSetMsg}
import cmdaa.ui.state.FileManagerActions.DlFluentConfig
import cmdaa.ui.utils.strDt2display
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.{CmdaaLog, GrokRun}
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.raw.{Blob, BlobPropertyBag, HTMLAnchorElement, URL}
import org.scalajs.dom.window.confirm
import play.api.libs.json.Json

import scala.scalajs.js
import scala.util.{Failure, Success}

/**
 * The State of the RunManager Component
 *
 * @param grokRun the currently selected grokRun
 * @param objUrl  the generated Download URL
 * @param log     the currently selected CMDAA Log
 */
case class RunManagerState (
  grokRun: Var[Option[GrokRun]]  = Var(None),
  objUrl:  Var[Option[String]]   = Var(None),
  log:     Var[Option[CmdaaLog]] = Var(None)
) extends StateModel[RunManagerStateRO] {
  override def toRO: RunManagerStateRO =
    RunManagerStateRO(grokRun, objUrl)
}

/**
 * The State of the RunManager Component
 *
 * @param grokRun the currently selected GrokRun
 * @param objUrl  the generated D/L URL
 */
case class RunManagerStateRO (
  grokRun: Binding[Option[GrokRun]],
  objUrl:  Binding[Option[String]]
) extends StateModelRO

object RunManagerActions {
  /** Fired when a GrokRun has been selected and it's data loaded */
  case class GrokRunLoaded(log: CmdaaLog, grokRun: GrokRun)
  extends StateAction

  /** Action for opening/closing the GrokRerun Modal. */
  case class GrokRerunOpen(value: Boolean)
  extends StateAction

  /** Action for requestion FluentD Config Download. */
  case class DlFluentRequested(anchor: HTMLAnchorElement)
  extends StateAction

  /** Action for deleting a CmdaaLog */
  case class DeleteLog(log: CmdaaLog)
  extends StateAction

  /** Request to Delete the GrokRun */
  case class DeleteRun(log: CmdaaLog, grokRun: GrokRun)
  extends StateAction

  /** Request to Clear the selected Run */
  case object ClearRun extends StateAction

  /** Request to Start an EFK test instance */
  case object GenFluentd extends StateAction

  /** Request to Delete the EFK test instance */
  case object DelEsInstance extends StateAction

  /** Request to Deploy the configured DaemonSet */
  case object DeployDaemonSet extends StateAction

  /** Request to Stop/Delete a deployed DaemonSet */
  case object StopDaemonSet extends StateAction
}

/**
 * ActionHandler for RunManager Actions.
 *
 * @param runManagerState the writable RunManagerState
 * @param sessToken authentication token for communicating with S3 services.
 */
class RunManagerActionHandler (
  private val runManagerState: RunManagerState,
  private val sessToken: Var[SessionToken]
) extends ActionHandler {
  import AuthenticatedActions.CmdaaLogsChanged
  import RunManagerActions._

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case GrokRunLoaded(log, grokRun) => hndlGrokRunLoaded(log, grokRun)
    case DlFluentRequested(anchor)   => hndlDlFluentRequest(dispatcher, anchor)
    case DeleteLog(log)              => hndlDeleteLog(dispatcher, log)
    case DeleteRun(log, run)         => hndlDeleteRun(dispatcher, log, run)
    case CmdaaLogsChanged(logs)      => hndlCmdaaLogsChanged(dispatcher, logs)

    case ClearRun        => hndlClearRun()
    case GenFluentd      => hndlGenFluentd()
    case DelEsInstance   => hndlDelEsInstance()
    case DeployDaemonSet => hndlDeployDaemonSet()
    case StopDaemonSet   => hndlStopDaemonSet()

    case _ =>
  }

  /**
   * Handles the CmdaaLogsChanged StateAction. If the current CmdaaLog/GrokRun
   * are still available, update to the latest, if different. Otherwise, clear
   * the state.
   *
   * @param dispatcher action dispatcher
   * @param logs       the new list of CmdaaLogs
   */
  private def hndlCmdaaLogsChanged (
    dispatcher: Dispatcher,
    logs: List[CmdaaLog]
  ): Unit = {
    runManagerState.log.value.foreach { currentLog =>
      logs.find(_.id == currentLog.id).fold {
        dispatcher dispatch ClearRun
      } { newLog =>
        runManagerState.grokRun.value.foreach { currentRun =>
          newLog.runs.find(_.id == currentRun.id).fold {
            dispatcher dispatch ClearRun
          } { newRun =>
            if (newRun != currentRun)
              runManagerState.grokRun.value = Some(newRun)
          }
        }
      }
    }
  }

  /**
   * Handles the DLFluent Request. Acquires the current GrokRun and dispatches
   * DLFluentConfig as RunManagerState doesn't have all the data needed to
   * complete the request.
   *
   * @param dispatcher the action dispatcher
   * @param anchor     the HTML Anchor
   */
  private def hndlDlFluentRequest (
    dispatcher: Dispatcher,
    anchor: HTMLAnchorElement
  ): Unit = {
    runManagerState.grokRun.value.fold {
      println("No GrokRun Selected; How did we get here?!")
    } { grokRun =>
      dispatcher dispatch DlFluentConfig(grokRun, anchor)
    }
  }

  /** Handles the GrokRunLoaded action; populating the RunManager */
  private def hndlGrokRunLoaded(log: CmdaaLog, grokRun: GrokRun): Unit = {
    runManagerState.log.value = Some(log)
    runManagerState.grokRun.value = Some(grokRun)
    runManagerState.objUrl.value.foreach(URL.revokeObjectURL)

    val grokResp = GrokOutputResp(grokRun.groks.map { g =>
      GrokMessage(g.grok, g.logs.map(LogMessage.apply).toArray)
    })
    runManagerState.objUrl.value = Some(genObjUrl(grokResp))
  }

  /** Handles the DeleteLog action. */
  private def hndlDeleteLog(dispatcher: Dispatcher, log: CmdaaLog): Unit = {
    if (confirm(s"""Really delete CmdaaLog: "${log.id}"?""")) {
      //delLogS3(dispatcher, run)
      delLogGS(dispatcher, log.id)
    }
  }

  /** Handles the DeleteRun action */
  private def hndlDeleteRun (
    dispatcher: Dispatcher,
    log: CmdaaLog,
    run: GrokRun
  ): Unit = {
    if (confirm(s"""Really delete Run: "${strDt2display(run.submitted)}"?""")) {
      delRunS3(dispatcher, run)
      delRunGS(dispatcher, log.id, run)
    }
  }

  /** Deletes a GrokRun from a CmdaaLog in the GrokStore */
  private def delRunGS (
    dispatcher: Dispatcher,
    logId: String,
    run: GrokRun
  ): Unit = {
    GrokstoreDao
      .deleteGrokRun(sessToken.value, logId, run)
      .onComplete(
        _.fold({
          case err: HttpError =>
            System.err.println (
              s"""Unhandled HttpError Deleting Run from GrokStore:
                 |${err.status}/${err.sText} -
                 |${err.rText}"""
                .stripMargin.replaceAll("\n", "")
            )
          case UnauthenticatedError =>
            dispatcher dispatch LogoutAction
        }, { _ =>
          dispatcher dispatch ClearRun
        })
      )
  }

  /** Deletes a GrokRun from a CmdaaLog in S3 */
  private def delRunS3(dispatcher: Dispatcher, grokRun: GrokRun)
  : Unit = {
    LogDao
      .delete(sessToken.value, grokRun.id)
      .onComplete(
        _.fold({
          case err: HttpError =>
            System.err.println (
              s"""Unhandled HttpError Deleting RunOutput:
                 |${err.status}/${err.sText} -
                 |${err.rText}"""
                .stripMargin.replaceAll("\n", "")
            )
          case UnauthenticatedError =>
            dispatcher dispatch LogoutAction
        }, { _ =>
          dispatcher dispatch ClearRun
        })
      )
  }

  /** Deletes a CmdaaLog from the Grokstore */
  private def delLogGS(dispatcher: Dispatcher, logId: String)
  : Unit = {
    GrokstoreDao
      .deleteCmdaaLog(sessToken.value, logId)
      .onComplete(
        _.fold({
          case err: HttpError =>
            System.err.println (
              s"""Unhandled HttpError Deleting Log from GrokStore:
                 |${err.status}/${err.sText} -
                 |${err.rText}"""
                .stripMargin.replaceAll("\n", "")
            )
          case UnauthenticatedError =>
            dispatcher dispatch LogoutAction
        }, { _ =>
          dispatcher dispatch ClearRun
        })
      )
  }

  /** Handles the ClearRun action, clearing the GrokRun from the RunManager */
  private def hndlClearRun(): Unit = {
    runManagerState.log.value     = None
    runManagerState.grokRun.value = None
    runManagerState.objUrl.value.foreach(URL.revokeObjectURL)
    runManagerState.objUrl.value  = None
  }

  /** Generates a URL for Downloading a "file" generated in the client */
  private def genObjUrl(grokOutputResp: GrokOutputResp): String =
    URL.createObjectURL(new Blob (
      js.Array(Json.prettyPrint(Json.toJson(grokOutputResp))),
      BlobPropertyBag("application/json")
    ))

  /** Calls the GenerateFluentD workflow service. */
  private def hndlGenFluentd(): Unit = {
    for {
      log <- runManagerState.log.value
      run <- runManagerState.grokRun.value
    } {
      println(s"Generating FluentD for CMDAA Log: '${log.id}', Run: '${run.id}'")

      WorkflowDao
        .efkStart(log, run.id)
        .onComplete {
          case Success(Some(v)) =>
            println("WorkflowDao.genFluentD Response - " + v)
          case Success(None) =>
            println("Success None?")
          case Failure(ex: AjaxException) =>
            println(s"Error in WorkflowDao.generateFluentdMongo - ${ex.getMessage}")
          case Failure(ex) =>
            println(s"Error: ${ex.getMessage}")
        }
    }
  }

  /** Calls the DeployDaemonSet workflow service. */
  private def hndlDeployDaemonSet(): Unit = {
    for {
      log <- runManagerState.log.value
      run <- runManagerState.grokRun.value
    } {
      println("Starting up DaemonSet for CMDAA Log: "
        + s"'${log.id}', Run: '${run.id}'")

      WorkflowDao
        .deployDaemonSet(DeployDaemonSetMsg(log.id, run.id))
        .onComplete {
          case Success(Some(v)) =>
            println("WorkflowDao.deployDaemonSet Response - " + v)
          case Success(None) =>
            println("Success: 'None'?")
          case Failure(ex: AjaxException) =>
            println(s"Error in WorkflowDao.generateFluentdMongo - ${ex.getMessage}")
          case Failure(ex) =>
            println(s"Error: ${ex.getMessage}")
        }
    }
  }

  /** Calls the StopDaemonSet Workflow */
  private def hndlStopDaemonSet(): Unit = {
    for {
      log <- runManagerState.log.value
      run <- runManagerState.grokRun.value
    } {
      println("Stop DaemonSet for CMDAA Log: "
        + s"'${log.id}', Run: '${run.id}'")

      WorkflowDao
        .stopDaemonSet(StopDaemonSetMsg(log.id, run.id))
        .onComplete {
          case Success(Some(v)) =>
            println("WorkflowDao.stopDaemonSet Response - " + v)
          case Success(None) =>
            println("Success: 'None'?")
          case Failure(ex: AjaxException) =>
            println(s"Error in WorkflowDao.generateFluentdMongo - ${ex.getMessage}")
          case Failure(ex) =>
            println(s"Error: ${ex.getMessage}")
        }
    }
  }

  /** Calls the "Delete ES Instance" workflow service. */
  private def hndlDelEsInstance(): Unit = {
    for {
      log <- runManagerState.log.value
      run <- runManagerState.grokRun.value
    } {
      println(s"Deleting ESi for CMDAA Log: '${log.id}', Run: '${run.id}'")

      WorkflowDao
        .efkDelete(log.id, run.id)
        .onComplete {
          case Success(Some(v)) =>
            println("WorkflowDao.deleteEsInstance Response - " + v)
          case Success(None) =>
            println("Success None?")
          case Failure(ex: AjaxException) =>
            println("Error in WorkflowDao.deleteEsInstance - " + ex.getMessage)
          case Failure(ex) =>
            println("Error: " + ex.getMessage)
        }
    }
  }
}
