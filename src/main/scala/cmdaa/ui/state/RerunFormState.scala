/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.dao.GrokstoreDao
import cmdaa.ui.dispatcher._
import cmdaa.ui.state.AuthenticatedActions.StartWorkflow
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.CmdaaLog
import io.cmdaa.gs.api.req.NewGrokRun
import org.scalajs.dom.document
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.raw.HTMLInputElement

/**
 * Client State for the ReRun workflow form.
 *
 * @param selCmdaaLog  the selected CMDAA Log
 * @param llThreshold  the llThreshold to configure
 * @param cosThreshold the cosThreshoold to configure
 */
case class RerunFormState (
  selCmdaaLog:  Var[Option[CmdaaLog]],
  llThreshold:  Var[Double] = Var(RerunFormState.DEF_LL_THRESH),
  cosThreshold: Var[Double] = Var(RerunFormState.DEF_COS_THRESH),
) extends StateModel[RerunFormStateRO] {
  override def toRO: RerunFormStateRO =
    RerunFormStateRO(llThreshold, cosThreshold)

  protected[state] def clear(): Unit = {
    llThreshold.value  = RerunFormState.DEF_LL_THRESH
    cosThreshold.value = RerunFormState.DEF_COS_THRESH
  }
}
object RerunFormState {
  val DEF_LL_THRESH:  Double = 0.06
  val DEF_COS_THRESH: Double = 0.9
}

/**
 * Read-Only State for the RerunForm.
 *
 * @param llThreshold  the llThreshold to configure
 * @param cosThreshold the cosThreshold to configure
 */
case class RerunFormStateRO (
  llThreshold:  Binding[Double],
  cosThreshold: Binding[Double],
) extends StateModelRO

/** Namespacing for the RerunForm Actions. */
object RerunFormActions {
  /** LLThreshold changed Action */
  case class LlThresholdChanged(txt: String) extends StateAction

  /** CosThreshold changed Action */
  case class CosThresholdChanged(txt: String) extends StateAction

  /** Rerun Form Submit Action */
  case object RerunFormSubmit extends StateAction

  /** Rerun Form Clear Action */
  case object RerunFormClear extends StateAction
}

/**
 * The ReRunForm Action Handler
 *
 * @param rerunFormState the writable ReRunForm State
 */
class RerunFormActionHandler (
  private val rerunFormState: RerunFormState,
  private val sessToken:      Var[SessionToken]
) extends ActionHandler {
  import RerunFormActions._

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
  import scala.util.{Failure, Success, Try}

  override def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    //// Form Actions
    case RerunFormSubmit => hndlFormSubmit(dispatcher)
    case RerunFormClear  => rerunFormState.clear()

    //// Open/Close Modal
    case AuthenticatedActions.RerunModalOpen(true)  => hndlModalOpen()
    case AuthenticatedActions.RerunModalOpen(false) => rerunFormState.clear()

    //// Change Handlers ////
    case LlThresholdChanged(txt) =>
      Try(txt.trim.toDouble).foreach(dbl =>
        rerunFormState.llThreshold.value = dbl)

    case CosThresholdChanged(txt) =>
      Try(txt.trim.toDouble).foreach(dbl =>
        rerunFormState.cosThreshold.value = dbl)

    case _ =>
  }

  /** Form-Submit handler */
  private def hndlFormSubmit(dispatcher: Dispatcher): Unit = {
    rerunFormState.selCmdaaLog.value.fold {
      println("No log file selected...")
    } { log =>
      println(s"Submitted the Rerun form for CMDAA Log '${log.path}'")

      val newGrokRun = NewGrokRun (
        llThreshold  = rerunFormState.llThreshold.value,
        cosThreshold = rerunFormState.cosThreshold.value
      )

      GrokstoreDao
        .insertGrokRun(sessToken.value, log.id, newGrokRun)
        .andThen {
          case Success(cmdaaLog) =>
            cmdaaLog.runs
              .find { gr =>
                gr.logLikelihood == newGrokRun.llThreshold &&
                gr.cosSimilarity == newGrokRun.cosThreshold
              }
              .fold {
                println("Couldn't find recently inserted GrokRun")
              } { insertedGrokRun =>
                dispatcher dispatch StartWorkflow(cmdaaLog, insertedGrokRun)
              }
          case _ =>
        }
        .onComplete {
          case Success(cmdaaLog) =>
            // TODO: do something with the latest CmdaaLog
            println(s"New GrokRun inserted for log: ${log.id}")
          case Failure(ex: AjaxException) =>
            println(s"Error creating new GrokRun in GrokStore/CmdaaLog ${log.id}. ${ex.getMessage}")
          case Failure(ex) =>
            println(s"Error creating new GrokRun in GrokStore/CmdaaLog ${log.id}. ${ex.getMessage}")
        }

      dispatcher dispatch AuthenticatedActions.RerunModalOpen(false)
    }
  }

  /** Modal-Open handler */
  private def hndlModalOpen(): Unit = {
    Option(document.getElementById("rerunLlThreshold"))
      .foreach { case input: HTMLInputElement => input.focus() }
  }
}
