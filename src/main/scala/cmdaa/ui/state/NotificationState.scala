/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.dispatcher._
import com.thoughtworks.binding.Binding.{BindingSeq, Vars}
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Node

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration
import scala.scalajs.js

/** Types of Notification */
object NotificationType extends Enumeration {
  type NotificationType = Value
  val Success, Warning, Error = Value
}

/**
 * Notification data
 *
 * @param notifType notification type
 * @param title     notification's title
 * @param content   notification's content
 * @param posted    time in millis of the notification's posting
 */
case class Notification (
  notifType: NotificationType.NotificationType,
  title:     String,
  content:   NodeBinding[Node],
  posted:    Long = System.currentTimeMillis()
)

/**
 * Application Notification State.
 *
 * @param notifications the notifications to display to the user.
 */
case class NotificationState (
  notifications: Vars[Notification] = Vars.empty,
) extends StateModel[NotificationStateRO] {
  override def toRO: NotificationStateRO =
    NotificationStateRO(notifications)
}

/**
 * The "read only" representation of NotificationState.
 *
 * @param notifications the notifications to display to the user.
 */
case class NotificationStateRO (
  notifications: BindingSeq[Notification]
) extends StateModelRO

object NotificationActions {
  /** Signal for pushing a new notification. */
  case class PushNotificationAction(notif: Notification) extends StateAction

  /** Signal for removing a previously pushed notification. */
  case class RemoveNotificationAction(notif: Notification) extends StateAction
}

/**
 * ActionHandler for Notifications.
 *
 * @param notificationState client-side notification state
 */
class NotificationActionHandler (
  private val notificationState: NotificationState
) extends ActionHandler {
  import NotificationActions._

  override protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case PushNotificationAction(notif) => pushNotification(dispatcher, notif)
    case RemoveNotificationAction(notif) => removeNotificationAction(notif)
    case _ =>
  }

  /**
   * Pushes a new Notification onto the "stack".
   *
   * @param dispatcher signal dispatcher
   * @param notif      notification to append
   */
  private def pushNotification(dispatcher: Dispatcher, notif: Notification)
  : Unit = {
    notificationState.notifications.value.prepend(notif)
    js.timers.setTimeout(FiniteDuration(30, TimeUnit.SECONDS)) {
      dispatcher dispatch RemoveNotificationAction(notif)
    }
  }

  private def removeNotificationAction(notif: Notification): Unit = {
    // This naive implementation will cause scheduled tasks to repeat this
    // instruction. However, this appears to be harmless.
    notificationState.notifications.value -= notif
  }
}
