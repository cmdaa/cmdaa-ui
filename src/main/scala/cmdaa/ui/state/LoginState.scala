/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.AppActions.{AjaxErrorSignal, LoginAction}
import cmdaa.ui.dao.AuthDao
import cmdaa.ui.dispatcher._
import cmdaa.ui.state.UnauthenticatedActions.LoginFailed
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.req.LoginReq
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Node
import org.scalajs.dom.ext.AjaxException

/**
 * The mutable state for the Login Form
 *
 * @param username the name of the user to login as
 * @param password the user's password
 */
case class LoginState (
  username: Var[String] = Var(""),
  password: Var[String] = Var("")
) extends StateModel[LoginStateRO] {
  override def toRO: LoginStateRO =
    LoginStateRO(username, password)

  /** Clears the form */
  protected[state] def clear(): Unit = {
    username.value = ""
    password.value = ""
  }

  /** Converts form data into a message for the authentication service. */
  def toMsg: LoginReq =
    LoginReq(username.value, password.value)
}

/**
 * The immutable representation of the Login Form data.
 *
 * @param username the name of the user to login as
 * @param password the user's password
 */
case class LoginStateRO(
  username: Binding[String],
  password: Binding[String]
) extends StateModelRO

/** LoginForm specific actions handled by the LoginActionHandler */
object LoginActions {
  /** Login Form username changed event */
  case class LoginUsernameChanged(txt: String) extends StateAction

  /** Login Form password changed event */
  case class LoginPasswordChanged(txt: String) extends StateAction

  /** Login Form submission event */
  case object LoginFormSubmit extends StateAction

  /** Login Form clear event */
  case object LoginFormClear  extends StateAction
}

/**
 * ActionHandler for the Login Form. Reacts to LoginForm Actions as well as
 * Application Actions regarding the LoginModal.
 *
 * @param loginState the mutable LoginForm State
 */
class LoginActionHandler (
  private val loginState: LoginState
) extends ActionHandler {
  import LoginActions._
  import org.scalajs.dom.document
  import org.scalajs.dom.raw.HTMLInputElement

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
  import scala.util.{Failure, Success}

  override def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    //// Primary Modal Actions ////
    case UnauthenticatedActions.LoginOpen(true)  => hndlLoginOpen()
    case UnauthenticatedActions.LoginOpen(false) => hndlLoginClose()
    case LoginFormSubmit => hndlLoginSubmit(dispatcher)
    case LoginFormClear  => loginState.clear()

    //// Change Handlers ////
    case LoginUsernameChanged(txt) => loginState.username.value = txt
    case LoginPasswordChanged(txt) => loginState.password.value = txt

    // Ignore unknown messages
    case _ =>
  }

  /** Actions to take when opening the login modal */
  private def hndlLoginOpen(): Unit = {
    Option(document.getElementById("loginUsername"))
      .foreach { case input: HTMLInputElement => input.focus() }
  }

  /** Actions to take when closing the login modal */
  private def hndlLoginClose(): Unit = {
    loginState.clear()
  }

  /**
   * Handles Submit for the LoginForm. Sends a login request to the Auth
   * Service.
   *
   * @param dispatcher app dispatcher
   */
  private def hndlLoginSubmit(dispatcher: Dispatcher): Unit = {
    val loginMsg = loginState.toMsg

    AuthDao.login(loginMsg)
      .onComplete {
        case Success(Some(loginResp)) =>
          dispatcher dispatch LoginAction(loginResp.token)
        case Success(None) =>
          println("SUCCESS/NONE on Login?")
        case Failure(ex: AjaxException) if ex.xhr.status == 401 =>
          System.err.println("401 RECECIEVED! Assume username/password is wrong")
          dispatcher dispatch LoginFailed(loginMsg)
        case Failure(ajaxEx: AjaxException) =>
          dispatcher dispatch AjaxErrorSignal(ajaxEx)
        case Failure(ex) =>
          println("Non-AjaxException? " + ex.getMessage)
      }
  }
}

object LoginNotifications {
  import cmdaa.ui.utils.intellijBindingHelper

  def loginFailed(loginReq: LoginReq): Notification =
    Notification (
      NotificationType.Error,
      "Login Failed!",
      loginFailedBody(loginReq)
    )

  @html
  private def loginFailedBody(loginReq: LoginReq): NodeBinding[Node] = {
    <span><strong>Authentication failed</strong> for username:
      <b>"{ loginReq.username }"</b>.
      Try again, or <em>Sign Up</em> if you don't have an account.</span>
  }
}
