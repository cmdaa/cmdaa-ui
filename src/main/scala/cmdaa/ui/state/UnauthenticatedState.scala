/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.state

import cmdaa.ui.AppActions
import cmdaa.ui.AppActions.{BadRespSignal, InvalidPkiSignal, LoginAction, ServiceErrSignal}
import cmdaa.ui.dao.{AuthDao, ServiceErrResp}
import cmdaa.ui.dispatcher._
import cmdaa.ui.state.NotificationActions.PushNotificationAction
import cmdaa.ui.state.SignupActions.SeedForm
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.Var
import io.cmdaa.auth.api.CmdaaUser
import io.cmdaa.auth.api.resp.UnregisteredPkiResp
import io.cmdaa.auth.api.req.LoginReq

/** Mutable data for the Application's "Unauthenticated" state */
case class UnauthenticatedState()
  extends StateModel[UnauthenticatedStateRO]
{
  /** Whether or not we've been deployed to a PKI-enabled environment */
  val pkiEnabled_? : Var[Boolean] = Var(true)

  /** Whether or not the Signup Modal is open */
  val signupOpen_? : Var[Boolean] = Var(false)

  /** Whether or not the Login Modal is open */
  val loginOpen_? : Var[Boolean] = Var(false)

  /** The Signup Form's State */
  val signupState: SignupState = SignupState(pkiEnabled_?)

  /** The Login Form's State */
  val loginState: LoginState = LoginState()

  override def toRO: UnauthenticatedStateRO =
    UnauthenticatedStateRO (
      pkiEnabled_?, signupOpen_?, loginOpen_?,
      signupState.toRO, loginState.toRO
    )
}

/**
 * The immutable state for the Application's "Unauthenticated" state.
 *
 * @param signupOpen_? Whether or not the Signup Modal is open
 * @param loginOpen_?  Whether or not the Login Modal is open
 * @param signupState  Current state of the Signup Form
 * @param loginState   Current state of the Login Form
 */
case class UnauthenticatedStateRO (
  pkiEnabled_? : Binding[Boolean],
  signupOpen_? : Binding[Boolean],
  loginOpen_?  : Binding[Boolean],
  signupState  : SignupStateRO,
  loginState   : LoginStateRO
) extends StateModelRO

/**
 * Application-wide Actions that are only available while the App is in an
 * "Unauthenticated" state.
 */
object UnauthenticatedActions {
  /** Dispatched when the server doesn't receive certs on PkiLogin */
  case class PkiDisabled(modalAction: StateAction) extends StateAction

  /** Starts the Login "workflow". Try PKI then open LoginDialog */
  case object StartLogin extends StateAction

  /** Starts the Signup "workflow". Try PKI then open SignupDialog */
  case object StartSignup extends StateAction

  /** Unauth App Action for opening/closing of the Signup Modal */
  case class SignupOpen(value: Boolean) extends StateAction

  /** Unauth App Action for opening/closing of the Login Modal */
  case class LoginOpen(value: Boolean) extends StateAction

  /** Unauth App Event signaling the creation of a new User */
  case class UserCreated(user: CmdaaUser) extends StateAction

  /** Unauth App Event signaling Login Failure. */
  case class LoginFailed(loginReq: LoginReq) extends StateAction
}

/**
 * ActionHandler for Application-wide Actions that are only available while
 * the App is in an "Unauthenticated" state.
 *
 * @param unauthState the mutable Authenticated State
 */
class UnauthenticatedActionHandler (
  private val unauthState: UnauthenticatedState
) extends ActionHandler {
  import UnauthenticatedActions._
  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

  override val subHandlers = List (
    new LoginActionHandler(unauthState.loginState),
    new SignupActionHandler(unauthState.signupState)
  )

  override protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit] = {
    case StartLogin  => hndlStartLogin(dispatcher)
    case StartSignup => hndlStartSignup(dispatcher)
    case PkiDisabled(modalAction) => hndlPkiDisabled(dispatcher, modalAction)

    case SignupOpen(newState)  => hndlSignupOpen(dispatcher, newState)
    case LoginOpen(newState)   => hndlLoginOpen(dispatcher, newState)
    case LoginFailed(loginReq) => hndlFailedLogin(dispatcher, loginReq)

    case _: UserCreated => dispatcher dispatch SignupOpen(false)
    case _: LoginAction => dispatcher dispatch LoginOpen(false)

    // Don't panic
    case _ =>
  }

  /**
   * Starts the Login "workflow":
   *
   *  1) try PKI authentication;
   *  2) if no PKI, open login modal;
   *  3) failed PKI - notify user (disallow access?)
   *
   * @param dispatcher app dispatcher
   */
  private def hndlStartLogin(dispatcher: Dispatcher): Unit = {
    dispatcher dispatch AppActions.MainNavOpen(false)

    if (unauthState.pkiEnabled_?.value) {
      AuthDao.loginPki() foreach {
        _.fold({ resp =>
          resp.status match {
            case 401 => dispatcher dispatch PkiDisabled(LoginOpen(true))
            case 403 => dispatcher dispatch InvalidPkiSignal(resp)
            case 404 => registerPkiUser(dispatcher, resp)
            case _   => dispatcher dispatch ServiceErrSignal(resp)
          }
        }, { loginResp =>
          dispatcher dispatch AppActions.LoginAction(loginResp.token)
        })
      }
    } else {
      dispatcher dispatch LoginOpen(true)
    }
  }

  /**
   * User clicked on Signup. App may or may not be PKI enabled. Therefore,
   * behave similarly to StartLogin; but open up the signup dialog if PKI is
   * not enabled.
   *
   * @param dispatcher app dispatcher
   */
  private def hndlStartSignup(dispatcher: Dispatcher): Unit = {
    dispatcher dispatch AppActions.MainNavOpen(false)

    if (unauthState.pkiEnabled_?.value) {
      AuthDao.loginPki() foreach {
        _.fold({ resp =>
          resp.status match {
            case 401 => dispatcher dispatch PkiDisabled(SignupOpen(true))
            case 403 => dispatcher dispatch InvalidPkiSignal(resp)
            case 404 => registerPkiUser(dispatcher, resp)
            case _   => dispatcher dispatch ServiceErrSignal(resp)
          }
        }, { loginResp =>
          dispatcher dispatch AppActions.LoginAction(loginResp.token)
        })
      }
    } else {
      dispatcher dispatch SignupOpen(true)
    }
  }

  /**
   * Records that PKI is not enabled; Dispatches the given action
   *
   * @param dispatcher app dispatcher
   * @param modalAction action to dispatch to open the correct modal
   */
  private def hndlPkiDisabled(dispatcher: Dispatcher, modalAction: StateAction)
  : Unit = {
    println("Environment not setup for PKI. Using username/password instead.")
    unauthState.pkiEnabled_?.value = false
    dispatcher dispatch modalAction
  }

  /**
   * Handles the opening/closing of the Signup Modal.
   *
   * @param dispatcher app dispatcher
   * @param newState   whether to open or close the modal
   */
  private def hndlSignupOpen(dispatcher: Dispatcher, newState: Boolean)
  : Unit = {
    unauthState.signupOpen_?.value = newState
    dispatcher dispatch AppActions.MainNavOpen(false)
  }

  /**
   * Handles the opening/closing of the Login Modal.
   *
   * @param dispatcher app dispatcher
   * @param newState   new modal state
   */
  private def hndlLoginOpen(dispatcher: Dispatcher, newState: Boolean)
  : Unit = {
    unauthState.loginOpen_?.value = newState
    dispatcher dispatch AppActions.MainNavOpen(false)
  }

  /**
   * Handles the "LoginFailed" signal. Called when the username/password is
   * incorrect.
   *
   * @param dispatcher app dispatcher
   * @param loginReq   the login request from the client
   */
  private def hndlFailedLogin(dispatcher: Dispatcher, loginReq: LoginReq)
  : Unit = {
    unauthState.loginOpen_?.value = false
    dispatcher dispatch PushNotificationAction (
      LoginNotifications.loginFailed(loginReq)
    )
  }

  /**
   * Parses the body of a HttpErrJsResp for user information. If successful,
   * seed the Signup Form with pre-populated fields and display it.
   *
   * @param dispatcher app dispatcher
   * @param resp       HttpErrJsResp containing UnregisteredPkiResp
   */
  private def registerPkiUser(dispatcher: Dispatcher, resp: ServiceErrResp)
  : Unit = {
    resp.body
      .flatMap(_.asOpt[UnregisteredPkiResp])
      .fold {
        dispatcher dispatch BadRespSignal(resp)
      } { pkiResp =>
        dispatcher dispatch SeedForm(pkiResp.userInfo)
      }
  }
}
