/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import cmdaa.ui.components.{MainNav, ModalForm, NotificationComponent}
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.forms.{EditGrokForm, DaemonConfigForm, LoginForm, RerunForm, SignupForm}
import cmdaa.ui.pages.{HomePage, NotFoundPage, UnauthHomePage}
import cmdaa.ui.router.{Route, Routes}
import cmdaa.ui.state._
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.raw._

/** The "root" component for the CMDAA-UI Application */
object App {

  /**
   * Base application layout, including the header, footer, primary navigation
   * elements; and defines the space for each page's contents to appear in.
   *
   * @param appState   the ApplicationState
   * @param dispatcher the Action/Event Dispatcher
   *
   * @return the base-application-layout component
   */
  @html
  def component(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val routes = appRoutes(appState)

    <div id="cmdaa-ui">
      <header class="hero is-dark is-bold">
        <div class="hero-body">
          <div class="container">
            <h1 class="title">CMDAA</h1>
            <h2 class="subtitle">Look at them <em>Metrics</em>!</h2>
          </div>
        </div>
      </header>
      <main class="page-body">
        { // Primary Navigation
          MainNav.component(appState).bind
        }
        { // Application-Wide Notification System
          appNotifications(appState).bind
        }
        { // Content of Current Page
          routes.bind.route(appState).bind
        }
        { // Application-Wide Modals for the current Authentication State
          appState.authState.bind.fold(
            unauth => unauthModals(unauth),
            auth   => authModals(auth)
          ).bind
        }
      </main>
      <footer class="footer">
        <div class="container">
          <div class="content has-text-centered">
            <p><strong>CMDAA-UI</strong>. Licensed under Apache 2.0</p>
          </div>
        </div>
      </footer>
    </div>
  }

  /**
   * Internal component for displaying Application Notifications.
   *
   * @param appState   the app's state
   * @param dispatcher the app dispatcher
   *
   * @return the app's notifications component
   */
  @html
  private def appNotifications(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    val notifications = appState.notifState.notifications

    val notifsClass = Binding {
      if (notifications.isEmpty.bind) "is-hidden" else ""
    }

    <section id="app-notifications" class={notifsClass.bind}>
      <div class="container">{
        notifications.mapBinding(NotificationComponent.component)
      }</div>
    </section>
  }

  /**
   * Produces a Binding of Routes depending on the current
   * "AppState.authState". Enables Auth-Aware Routing for the App.
   *
   * @param appState the ApplicationState
   * @return a Binding of Routes
   */
  private def appRoutes(appState: AppStateRO)
  : Binding[Routes[AppStateRO]] = Binding {
    appState.authState.bind.fold (
      _ => /* unauthenticated */
        Routes[AppStateRO](appState.route, NotFoundPage)(
          "/"     -> UnauthHomePage,
          "/docs" -> DocsPage
        ),
      _ => /* authenticated */
        Routes[AppStateRO](appState.route, NotFoundPage)(
          "/"     -> HomePage,
          "/docs" -> DocsPage
        )
    )
  }

  /** Modals used while the Application is in an Authenticated State */
  @html
  private def authModals(authState: AuthenticatedStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <div id="modals">
      {
        ModalForm.component (
          "rerunForm",
          "Grok Rerun", "Run!",
          authState.rerunModalOpen_?,
          AuthenticatedActions.RerunModalOpen.apply
        )(RerunForm.component(authState.rerunFormState)).bind
      } {
        ModalForm.component (
          "grokEditForm",
          "Grok Edit", "Save",
          authState.geditModalOpen_?,
          AuthenticatedActions.GeditModalOpen.apply,
          isWide = true
        )(EditGrokForm.component(authState.editGrokFormState)).bind
      } {
        ModalForm.component (
          "daemonsetConfigForm",
          "DaemonSet Config", "Save",
          authState.daemonSetModalOpen_?,
          AuthenticatedActions.DsConfigOpen.apply
        )(DaemonConfigForm.component(authState.dsConfigFormState)).bind
      }
    </div>

  /** Modals used while the Application is in an Unauthenticated State */
  @html
  private def unauthModals(unauthState: UnauthenticatedStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <div id="modals">{
      ModalForm.component (
        "signupForm",
        "Sign Up", "Sign Up!",
        unauthState.signupOpen_?,
        UnauthenticatedActions.SignupOpen.apply
      )(SignupForm.component(unauthState.signupState)).bind
    } {
      ModalForm.component (
        "loginForm",
        "Login", "Login!",
        unauthState.loginOpen_?,
        UnauthenticatedActions.LoginOpen.apply
      )(LoginForm.component(unauthState.loginState)).bind
    }</div>
}

object DocsPage extends Route[AppStateRO] {

  /**
   * An example (for now) "documentation" page for demonstrating how the app
   * is setup for "paging".
   *
   * @param appState the application state
   * @param dispatcher Action Dispatcher
   *
   * @return the "Documentation" page component
   */
  @html
  override def component(appState: AppStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] =
    <section class="section">
      <h1>DOCUMENTS PAGE</h1>
    </section>
}
