/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import cmdaa.ui.AppActions.LoginAction
import cmdaa.ui.dao.SessionTokenDao
import cmdaa.ui.dispatcher._
import com.thoughtworks.binding.Binding.Var
import org.lrng.binding.html
import org.scalajs.dom.{Event, document, window}

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExportTopLevel, JSImport}

@JSImport("@fortawesome/fontawesome-free/js/all.js", JSImport.Default)
@js.native
object FontJs extends js.Object

@JSImport("sass/main.scss", JSImport.Default)
@js.native
object MainCss extends js.Object

object Main {
  // Load static assets
  val fontJs:  FontJs.type  = FontJs
  val mainCss: MainCss.type = MainCss

  @JSExportTopLevel("main")
  def main(): Unit = {
    val container = Option(document.getElementById("root"))
      .getOrElse {
        val elem = document.createElement("div")
        elem.id = "root"
        document.body.appendChild(elem)
        elem
      }

    val appState = AppState(Var(window.location.pathname))

    implicit val dispatcher: Dispatcher = Dispatcher (
      new AppActionHandler(appState) )

    window.onpopstate = { e: Event =>
      e.preventDefault()
      dispatcher dispatch AppActions.PathUpdate
    }

    html.render(container, App.component(appState.toRO))
    SessionTokenDao.load().foreach(t => dispatcher.dispatch(LoginAction(t)))
  }
}
