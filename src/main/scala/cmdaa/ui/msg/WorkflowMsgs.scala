/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.msg

object WorkflowMsgs {
  case class LlWorkflowMsg (
    logId:        String,
    runId:        String,
    path:         String,
    llThreshold:  Double,
    cosThreshold: Double,
    bucket:       String = "cmdaa",
    analytic:     String = "hadoop",
    logSourceId:  Long   = 1
  ) {
    lazy val s3InputFile: String = {
      val lastSlash = path.lastIndexOf("/")
      if (lastSlash > -1) path.substring(lastSlash+1) else path
    }

    lazy val s3Folder: String = {
      val lastSlash = path.lastIndexOf("/")
      if (lastSlash > -1) path.substring(0, lastSlash) else "/"
    }
  }

  case class DeployDaemonSetMsg(logId: String, runId: String)
  case class StopDaemonSetMsg(logId: String, runId: String)
}
