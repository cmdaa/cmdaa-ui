/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.msg

import play.api.libs.json.{Format, Json}

object LogMsgs {

  /**
   * The current state of the GrokRun.
   *
   * @param isRunning  Whether or not the GrokRun is currently running
   * @param isComplete Whether or not the GrokRun has been completed
   * @param IsError    Whether or not the GrokRun terminated with an Error
   * @param message    The current message from the GrokRun
   */
  case class RunStatus (
    isRunning:  Boolean,
    isComplete: Boolean,
    IsError:    Boolean,
    message:    String
  )
  object RunStatus {
    implicit val format: Format[RunStatus] = Json.format
  }

  /**
   * Data about Workflow Runs against log files.
   *
   * @param path   full S3 path of the RunData
   * @param runId  the ID of the RunData (time in Millis of when it was run)
   * @param status the status of the Run
   */
  case class RunData (
    path:   String,
    runId:  Long,
    status: RunStatus
  )
  object RunData {
    implicit val format: Format[RunData] = Json.format
  }

  /**
   * Data about uploaded log files, their runs, etc...
   *
   * @param path         the full S3 path of the LogData
   * @param username     the name of User who owns the LogData
   * @param lastModified the time the Log was lastModified
   * @param logName      the name of the LogFile
   * @param runs     the Workflow Runs against the file
   */
  case class LogData (
    path:         String,
    username:     String,
    lastModified: Long,
    logName:      String,
    runs:         List[RunData]
  )
  object LogData {
    /** Default JSON serialization for LogData */
    implicit val format: Format[LogData] = Json.format
  }


  /**
   * File list messaage from the S3 Service.
   *
   * @param results files stored in "S3"
   */
  case class LogListResp(results: List[LogData])
  object LogListResp {
    /** Default JSON serialization for ListFilesMsg */
    implicit val format: Format[LogListResp] = Json.format
  }

  /**
   * Response from the S3 Service when a file is deleted
   *
   * @param file the deleted file's path
   */
  case class S3FileDeletedResp(file: String)
  object S3FileDeletedResp {
    implicit val format: Format[S3FileDeletedResp] = Json.format
  }

  /**
   * Success Message from the S3 Service in response to successfully uploading
   * a log file.
   *
   * @param fileName the name of the file in stored via the "S3" service.
   * @param bucket   name of the bucket the file was stored in
   */
  case class UploadedMsg(fileName: String, bucket: String)
  object UploadedMsg {
    /** Default JSON serialization for UploadedMsg */
    implicit val format: Format[UploadedMsg] = Json.format
  }


  //// Grok Output Messages ////

  case class GrokOutputResp(groks: List[GrokMessage])
  object GrokOutputResp {
    implicit val format: Format[GrokOutputResp] = Json.format
  }

  case class GrokMessage(grok: String, logMessages: Array[LogMessage])
  object GrokMessage {
    implicit val format: Format[GrokMessage] = Json.format
  }

  case class LogMessage(logMessage: String)
  object LogMessage {
    implicit val format: Format[LogMessage] = Json.format
  }
}
