/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dao

import cmdaa.ui.msg.LogMsgs
import cmdaa.ui.utils.{FileUploadCallbacks, Uploader}
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.resp.S3SvcResp
import org.scalajs.dom.File
import org.scalajs.dom.ext.{Ajax, AjaxException}
import org.scalajs.dom.raw.FormData
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.URIUtils.encodeURIComponent

object LogDao {
  /** Base URI for Log/File Services */
  private val fileSvcUri: String = "/gs/api/v1/files"

  /** Common request headers for interacting with the File Upload Service */
  private def reqHeaders(sessToken: SessionToken): Map[String, String] =
    Map("Authorization" -> s"Bearer ${sessToken.token}")

  /**
   * List of the user's uploaded log files.
   *
   * @param sessToken user credentials for using the file upload service
   *
   * @return asynchronous, optional message from the service with the file list
   */
  def listFiles(sessToken: SessionToken)
  : Future[LogMsgs.LogListResp] = {
    Ajax
      .get(fileSvcUri, headers = reqHeaders(sessToken))
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Json.parse(xhr.responseText).as[LogMsgs.LogListResp]
          case _ =>
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 =>
            throw UnauthenticatedError
          case _ =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Fetches an output file from the S3 Service.
   *
   * @param sessToken user credentials for using the S3 Service
   * @param path      the output path to fetch
   *
   * @return JSON of the output from the S3 Service
   */
  def getGrokOutput(sessToken: SessionToken, path: String)
  : Future[LogMsgs.GrokOutputResp] = {
    Ajax
      .get (
        s"$fileSvcUri/file?path=${encodeURIComponent(path)}",
        headers = reqHeaders(sessToken)
      )
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Json.parse(xhr.responseText).as[LogMsgs.GrokOutputResp]
          case _ =>
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /** Fetches a text file by Path from the S3 service */
  def fetchText(sessToken: SessionToken, path: String)
  : Future[String] = {
    Ajax
      .get (
        s"$fileSvcUri/file?path=${encodeURIComponent(path)}",
        headers = reqHeaders(sessToken)
      )
      .map { xhr =>
        xhr.status match {
          case 200 =>
            xhr.responseText
          case _ =>
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Delete a file from the S3 Service.
   *
   * @param sessToken user credentials for using the S3 Service
   * @param path      the output path to fetch
   * @return JSON of the output from the S3 Service
   */
  def delete(sessToken: SessionToken, path: String)
  : Future[S3SvcResp] = {
    Ajax
      .delete (
        s"$fileSvcUri/file?path=${encodeURIComponent(path)}",
        headers = reqHeaders(sessToken)
      )
      .map { xhr =>
        xhr.status match {
          case 200 => Json.parse(xhr.responseText).as[S3SvcResp]
          case _ => throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _   =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Calls the "FileUpload" service with the supplied credentials and logFile.
   * Attempts to parse the response and return it appropriately.
   *
   * @param sessToken user credentials for using the file upload service
   * @param logFile   the File to upload
   * @param lmDate    the LastModified Date (or "now" if LMD is not supported
   *                  by the browser)
   *
   * @return asynchronous, optional mesage from the service regarding the
   *         uploaded file
   */
  def upload (
    sessToken: SessionToken,
    logFile: File,
    lmDate: Long,
    callbacks: FileUploadCallbacks
  ): Future[S3SvcResp] = {
    val formData = new FormData()
    formData.append("file", logFile)
    formData.append("lmDate", lmDate.toDouble)

    Uploader
      .put(s"$fileSvcUri/log", formData,
        headers = reqHeaders(sessToken),
        callbacks = callbacks
      )
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Json.parse(xhr.responseText).as[S3SvcResp]
          case _ =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 =>
            throw UnauthenticatedError
          case _ =>
            println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }
}
