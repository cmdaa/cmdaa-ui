/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dao

import cmdaa.ui.msg.WorkflowMsgs.{DeployDaemonSetMsg, LlWorkflowMsg, StopDaemonSetMsg}
import io.cmdaa.gs.api.CmdaaLog
import org.scalajs.dom.ext.Ajax
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object WorkflowDao {
  /** Headers for JSON ContentType */
  private val jsContentTypeHeaders =
    Map("Content-Type" -> "application/json")

  /**
   * Calls the Cmdaa-Workflows-EventSource-Service to start a "run-ingest"
   * workflow.
   *
   * @param llWorkflowMsg parameters to send to the "run-ingest" workflow
   * @return
   */
  def runIngest(llWorkflowMsg: LlWorkflowMsg)
  : Future[Option[String]] = {
    val wfJsReq = Json.stringify(Json.obj (
      //"workflow-name" -> "run-ingest",
      "log-id" -> llWorkflowMsg.logId,
      "run-id" -> llWorkflowMsg.runId
    ))

    Ajax
      .post("/argo/run-ingest", wfJsReq, headers = jsContentTypeHeaders)
      .map { xhr =>
        if (xhr.status == 200) {
          Some(xhr.responseText)
        } else {
          println (
            "WorkflowDAO.runIngest RECEIVED - " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}"
          )
          None
        }
      }
  }

  /**
   * Calls the Cmdaa-Workflows-EventSource-Service to start a "efk-start"
   * workflow.
   *
   * @param log   the CMDAA Log to start the EFK instance for
   * @param runId the stringified BSON ID of the CmdaaLog.run
   *
   * @return the response from the service, if any.
   */
  def efkStart(log: CmdaaLog, runId: String)
  : Future[Option[String]] = {
    val wfJsReq = Json.stringify(Json.obj(
      //"workflow-name" -> "efk-start",
      "log-id" -> log.id,
      "run-id" -> runId
    ))

    Ajax
      .post("/argo/efk-start", wfJsReq, headers = jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Some(xhr.responseText)
          case _ =>
            println (
              "WorkflowDAO.efkStart() RECEIVED - " +
                s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}"
            )
            None
        }
      }
  }

  /**
   * Calls the Cmdaa-Workflows-EventSource-Service to start a "efk-delete"
   * workflow.
   *
   * @param logId CmdaaLog ID
   * @param runId GrokRun ID
   * @return response from the CMDAA Workflows EventSource Service
   */
  def efkDelete(logId: String, runId: String): Future[Option[String]] = {
    val wfJsReq = Json.stringify(Json.obj(
      //"workflow-name" -> "efk-delete",
      "log-id" -> logId,
      "run-id" -> runId
    ))

    Ajax
      .post("/argo/efk-delete", wfJsReq, headers = jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Some(xhr.responseText)
          case _ =>
            println (
              "WorkflowDAO.efkDelete() RECEIVED - " +
                s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}"
            )
            None
        }
      }
  }

  /**
   * Calls the Cmdaa-Workflows-EventSource-Service to start/deploy a
   * "daemon-set".
   *
   * @param workflowMsg data needed to deploy a daemon set
   * @return async response
   */
  def deployDaemonSet(workflowMsg: DeployDaemonSetMsg)
  : Future[Option[String]] = {
    val wfJsReq = Json.stringify(Json.obj(
      //"workflow-name" -> "start-fluentd-daemon",
      "log-id" -> workflowMsg.logId,
      "run-id" -> workflowMsg.runId
    ))

    Ajax
      .post("/argo/start-fluentd-daemon", wfJsReq, headers = jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Some(xhr.responseText)
          case _ =>
            println (
              "WorkflowDao.deployDaemonSet() RECEIVED - " +
                s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}"
            )
            None
        }
      }
  }

  /**
   * Calls the Cmdaa-Workflows-EventSource-Service to start/deploy a
   * "daemon-set".
   *
   * @param workflowMsg data needed to stop a deployed daemonset
   * @return
   */
  def stopDaemonSet(workflowMsg: StopDaemonSetMsg)
  : Future[Option[String]] = {
    val wfJsReq = Json.stringify(Json.obj(
      //"workflow-name" -> "stop-fluentd-daemon",
      "log-id" -> workflowMsg.logId,
      "run-id" -> workflowMsg.runId
    ))

    Ajax
      .post("/argo/stop-fluentd-daemon", wfJsReq, headers = jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Some(xhr.responseText)
          case _ =>
            println (
              "WorkflowDao.stopDaemonSet() RECEIVED - " +
                s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}"
            )
            None
        }
      }
  }
}
