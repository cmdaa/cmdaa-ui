/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dao

import io.cmdaa.auth.api.SessionToken
import org.scalajs.dom.ext.SessionStorage
import play.api.libs.json.Json

import scala.util.Try

/**
 * Manages the storage/loading of the User's Authentication Token.
 */
object SessionTokenDao {
  /** The name of the Session Storage key for saving/loading the AuthToken */
  private val localStorageName = "cmdaa-ui.session-token"

  /**
   * Attempts to load/parse a stored session token.
   *
   * @return some SessionToken if a valid one exists; None, otherwise.
   */
  def load(): Option[SessionToken] =
    Try {
      SessionStorage(localStorageName)
        .map(Json.parse(_).as[SessionToken])
    } getOrElse None

  /**
   * Saves the given SessionToken to the SessionStorage of the user's browser.
   *
   * @param sessToken the User's SessionToken to persist
   */
  def save(sessToken: SessionToken): Unit =
    SessionStorage(localStorageName) =
      Json.stringify(Json.toJson(sessToken))

  /**
   * Clears the stored SessionToken; ensures the User remains Logged Out on
   * reload.
   */
  def rmToken(): Unit =
    SessionStorage.remove(localStorageName)
}
