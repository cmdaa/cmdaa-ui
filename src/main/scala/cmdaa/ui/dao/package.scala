/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import org.scalajs.dom.raw.XMLHttpRequest
import play.api.libs.json.{JsValue, Json}

import scala.util.Try

package object dao {
  case object NotModified          extends Exception
  case object UnauthenticatedError extends Exception

  /**
   * Maybe replaced by ServiceErrResp???
   *
   * @param status http status   code
   * @param sText  http status   text
   * @param rText  http response text
   */
  case class HttpError (
    status: Int,
    sText:  String,
    rText:  String
  ) extends Exception

  /**
   * For capturing relavent data from HTTP Error Responses with JSON bodies.
   *
   * @param status     the http response status
   * @param sText the http response status text
   * @param body       the JsValue of the body
   */
  case class ServiceErrResp (
    status: Int,
    sText:  String,
    body:   Option[JsValue]
  )
  object ServiceErrResp {
    /** Creates a ServiceErrResp from a XMLHttpRequest */
    def fromErrResp(xhr: XMLHttpRequest): ServiceErrResp =
      ServiceErrResp (
        xhr.status,
        xhr.statusText,
        Option(xhr.responseText)
          .flatMap(t => Try(Json.parse(t)).toOption)
      )
  }
}
