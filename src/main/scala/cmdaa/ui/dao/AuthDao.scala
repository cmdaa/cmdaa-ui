/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dao

import io.cmdaa.auth.api.SessionToken
import io.cmdaa.auth.api.req.{LoginReq, SignupReq}
import io.cmdaa.auth.api.resp.{CmdaaUserResp, LoginResp}
import org.scalajs.dom.ext.{Ajax, AjaxException}
import play.api.libs.json.{JsValue, Json}

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object AuthDao {
  /** Common request headers for interacting with the Authentication Service */
  private val jsonHeaders: Map[String, String] =
    Map("Content-Type" -> "application/json")

  /** Common request headers for interacting with the Authentication Service */
  private def authHeaders(sessToken: SessionToken): Map[String, String] =
    Map("Authorization" -> s"Bearer ${sessToken.token}")

  /**
   * CMDAA-Auth signup.
   *
   * @param signupReq signup request information
   * @return async, optional CmdaaUser
   */
  def signup(signupReq: SignupReq): Future[Option[CmdaaUserResp]] = {
    Ajax.post (
      "/auth/api/v1/users",
      Json.stringify(Json.toJson(signupReq)),
      headers = jsonHeaders
    ) map { xhr =>
      xhr.status match {
        case 200 =>
          Some(Json.parse(xhr.responseText).as[CmdaaUserResp])
        case _ =>
          println(s"RECEIVED ${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
          None
      }
    }
  }

  /**
   * CMDAA-Auth login.
   *
   * @param loginReq login request data
   * @return async, optional LoginResp
   */
  def login(loginReq: LoginReq): Future[Option[LoginResp]] =
    Ajax.post (
      "/auth/api/v1/login",
      Json.stringify(Json.toJson(loginReq)),
      headers = jsonHeaders
    ) map { xhr =>
      xhr.status match {
        case 200 =>
          Some(Json.parse(xhr.responseText).as[LoginResp])
        case _ =>
          None
      }
    }

  /**
   * CMDAA-Auth PKI Login. Tries to authenticate user via PKI. Expected
   * response codes:
   *
   * 200 - user is logged in
   * 401 - no certificate received
   * 403 - certificate rejected
   * 404 - user not registered
   *
   * @return async result
   */
  def loginPki(): Future[Either[ServiceErrResp, LoginResp]] =
    Ajax
      .get("/auth/api/v1/login", headers = jsonHeaders)
      .map { xhr =>
        Right(Json.parse(xhr.responseText).as[LoginResp])
      }
      .recover { case aEx: AjaxException =>
        Left(ServiceErrResp.fromErrResp(aEx.xhr))
      }

  /**
   * Logs the user out of CMDAA, invalidating the current authentication
   * token.
   *
   * @return async result
   */
  def logout(sessToken: SessionToken)
  : Future[Either[ServiceErrResp, JsValue]] =
    Ajax
      .delete (
        "/auth/api/v1/login",
        headers = jsonHeaders ++ authHeaders(sessToken)
      )
      .map { xhr => Right(Json.parse(xhr.responseText)) }
      .recover { case aEx: AjaxException =>
        Left(ServiceErrResp.fromErrResp(aEx.xhr))
      }
}
