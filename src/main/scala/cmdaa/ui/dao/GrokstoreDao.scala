/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dao

import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api._
import io.cmdaa.gs.api.req._
import io.cmdaa.gs.api.resp.{FetchCmdaaLog, FetchLogList}
import org.scalajs.dom.ext.{Ajax, AjaxException}
import play.api.libs.json._

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object GrokstoreDao {
  /** Base URI for CmdaaLog Services */
  private val logSvcUri: String = "/gs/api/v1/logs"

  /** Headers for JSON ContentType */
  private val jsContentTypeHeaders =
    Map("Content-Type" -> "application/json")

  /** Common request headers for interacting with the Grokstore Service */
  private def reqHeaders(sessToken: SessionToken): Map[String, String] =
    Map("Authorization" -> s"Bearer ${sessToken.token}")

  /**
   * Fetches a list of CmdaaLogs from the Grokstore Service.
   *
   * @param sessToken authentication token
   * @return async list of CmdaaLogs
   */
  def listCmdaaLogs(sessToken: SessionToken)
  : Future[List[CmdaaLog]] = {
    Ajax
      .get(logSvcUri, headers = reqHeaders(sessToken))
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Json.parse(xhr.responseText).as[FetchLogList].logs
          case _ =>
            println("List CmdaaLogs - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("FetchCmdaaLogs - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Inserts a new CmdaaLog. Meant to be called after successfully uploading
   * a log file to S3.
   *
   * @param sessToken   the authentication token
   * @param newCmdaaLog the new CMDAA Log to insert
   * @return the updated CmdaaLog
   */
  def insertCmdaaLog(sessToken: SessionToken, newCmdaaLog: NewCmdaaLog)
  : Future[CmdaaLog] = {
    val insJsReq = Json.stringify(Json.toJson(InsertCmdaaLog(newCmdaaLog)))

    Ajax
      .post(logSvcUri, insJsReq, headers =
        reqHeaders(sessToken) ++ jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            Json.parse(xhr.responseText).as[FetchCmdaaLog].log
          case _ =>
            println("Insert CmdaaLog - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("InsertLog - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Deletes a CmdaaLog from the GrokStore.
   *
   * @param sessToken the authentication token
   * @param logId     the ID of the CMDAA Log to delete
   *
   * @return the result
   */
  def deleteCmdaaLog(sessToken: SessionToken, logId: String)
  : Future[Boolean] =
    Ajax
      .delete(s"$logSvcUri/$logId", headers = reqHeaders(sessToken))
      .map { xhr =>
        xhr.status match {
          case 200 =>
            println(s"CmdaaLog Deleted: $logId")
            true
          case _ =>
            println("DeleteLog - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            false
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("DeleteLog - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }

  /**
   * Set the LogLocation of a CmdaaLog.
   *
   * @param sessToken the authentication token
   * @param logId     the ID ofthe CMDAA Log to delete
   * @param loc       the LogLocation to set in the CmdaaLog.
   *
   * @return the result
   */
  def setDaemonSetConfig (
    sessToken: SessionToken,
    logId:     String,
    loc:       DsConfig
  ): Future[CmdaaLog] = {
    val jsReq = Json.stringify(Json.toJson(SetDaemonSetConfig(loc)))

    Ajax
      .put(s"$logSvcUri/$logId/dsConfig", jsReq, headers =
        reqHeaders(sessToken) ++ jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            println(s"LogLocation set in CmdaaLog $logId")
            Json.parse(xhr.responseText).as[FetchCmdaaLog].log
          case _ =>
            println("SetLogLocation - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("SetLogLocation - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Inserts a GrokRun into an existing CmdaaLog.
   *
   * @param sessToken  authentication token
   * @param logId      ID of the CmdaaLog to insert the GrokRun into
   * @param newGrokRun data for inserting a new GrokRun
   * @return the modified CmdaaLog
   */
  def insertGrokRun (
    sessToken:  SessionToken,
    logId:      String,
    newGrokRun: NewGrokRun
  ): Future[CmdaaLog] = {
    val insJsReq = Json.stringify(
      Json.toJson(InsertGrokRun(newGrokRun)))

    Ajax
      .post(s"$logSvcUri/$logId/runs", insJsReq, headers =
        reqHeaders(sessToken) ++ jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            println(s"New GrokRun inserted in CmdaaLog $logId")
            Json.parse(xhr.responseText).as[FetchCmdaaLog].log
          case _ =>
            println("InsertGrokRun - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("InsertGrokRun - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Updates a CmdaaLog/Run/Grok in the Grokstore.
   *
   * @param sessToken  the authentication token
   * @param logId      the ID of the CmdaaLog to update
   * @param grokRun    the ID of the Grokrun to update
   * @param grokIdx    the index of the Grokrun
   * @param grokStr the updated grok string
   *
   * @return the updated CmdaaLog
   */
  def updateGrok (
    sessToken: SessionToken,
    logId:     String,
    grokRun:   GrokRun,
    grokIdx:   Int,
    grokStr:   String
  ): Future[CmdaaLog] = {
    val updtJsReq = Json.stringify(
      Json.toJson(UpdateGrok(grokStr)))

    Ajax
      .put(s"$logSvcUri/$logId/runs/${grokRun.id}/groks/$grokIdx", updtJsReq,
        headers = reqHeaders(sessToken) ++ jsContentTypeHeaders)
      .map { xhr =>
        xhr.status match {
          case 200 =>
            println(s"Grok Updated: '$logId/runs/${grokRun.id}/groks/$grokIdx'")
            Json.parse(xhr.responseText).as[FetchCmdaaLog].log
          case _ =>
            println("UpdateGrok - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("InsertGrokRun - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }

  /**
   * Deletes a GrokRun from a CmdaaLog in the GrokStore.
   *
   * @param sessToken the authentication token
   * @param logId     the CMDAA Log to the GrokRun from
   * @param grokRun   the GrokRun to delete
   * @return the result
   */
  def deleteGrokRun (
    sessToken: SessionToken,
    logId:     String,
    grokRun:   GrokRun
  ): Future[Boolean] = {
    Ajax
      .delete(s"$logSvcUri/$logId/runs/${grokRun.id}",
        headers = reqHeaders(sessToken))
      .map { xhr =>
        xhr.status match {
          case 200 =>
            println(s"GrokRun Deleted: '$logId/runs/${grokRun.id}'")
            true
          case _ =>
            println("DeleteRun - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            false
        }
      }
      .recover { case AjaxException(xhr) =>
        xhr.status match {
          case 401 => throw UnauthenticatedError
          case _ =>
            println("DeleteRun - RECEIVED " +
              s"${xhr.status}/${xhr.statusText} - ${xhr.responseText}")
            throw HttpError(xhr.status, xhr.statusText, xhr.responseText)
        }
      }
  }
}
