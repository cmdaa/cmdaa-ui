/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.dispatcher

/**
 * Action Dispatcher used throughout the Application to send notifications of
 * events throughout the system to ActionHandlers who are interested in
 * receiving them.
 *
 * @param rootHandlers a list of "root" level ActionHandler to dispatch
 *                     Actions to
 */
class Dispatcher (
  private val rootHandlers: List[ActionHandler]
) {
  /**
   * Send an Action to ActionHandlers.
   *
   * @param action the Action to send.
   */
  def dispatch(action: StateAction): Unit =
    rootHandlers.foreach(_.handle(this, action))
}
object Dispatcher {
  /**
   * Utility Dispatcher creator that takes a vararg of ActionHandlers.
   *
   * @param handlers the ActionHandler to initialize the Dispatcher with
   * @return the new Dispatcher
   */
  def apply(handlers: ActionHandler*) =
    new Dispatcher(handlers.toList)
}

/**
 * ActionHandlers receive events, handle the events, then distribute those
 * events to any sub-ActionHandlers.
 */
trait ActionHandler {
  /** Sub ActionHandlers who handle Actions AFTER the parent ActionHandler */
  def subHandlers: List[ActionHandler] = Nil

  /**
   * Handles Actions.
   *
   * @param dispatcher  The ActionHandler for dispatching subsequent,
   *                    side-effect actions
   * @param stateAction The StateAction that needs to be Handled.
   */
  def handle(dispatcher: Dispatcher, stateAction: StateAction): Unit = {
    handle(dispatcher)(stateAction)
    subHandlers.foreach(_.handle(dispatcher, stateAction))
  }

  /**
   * Conenience override point for handling Actions
   *
   * @param dispatcher The ActionHandler for dispatching subsequent,
   *                   side-effect actions
   * @return the ActionHandling PartialFunction
   */
  protected def handle(dispatcher: Dispatcher)
  : PartialFunction[StateAction, Unit]
}
