/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.forms

import cmdaa.ui.components.FormInput
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.state.LoginActions._
import cmdaa.ui.state.LoginStateRO
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding._
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Event
import org.scalajs.dom.raw._

object LoginForm {

  /**
   * Constructs a LoginForm component.
   *
   * @param loginState the ReadOnly LoginState
   * @param dispatcher the Action Dispatcher
   *
   * @return Element Binding for the LoginForm
   */
  @html
  def component(loginState: LoginStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    <form
      id="loginForm"
      onsubmit={ e: Event =>
        e.preventDefault()
        dispatcher dispatch LoginFormSubmit
      }
    >
      {
        FormInput.textField (
          "loginUsername",
          "User Name",
          loginState.username,
          LoginUsernameChanged.apply,
          Vars.empty,

          required = true,

          maxLength = Some(100),
          leftIcon  = Var(Some("fas fa-user"))
        ).bind
      } {
        FormInput.textField (
          "loginPassword",
          "Password",
          loginState.password,
          LoginPasswordChanged.apply,
          Vars.empty,

          fieldType = "password",
          required  = true,

          maxLength = Some(100),
          leftIcon  = Var(Some("fas fa-key"))
        ).bind
      }

      <input type="submit" style="display: none" />
    </form>
  }
}
