/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import cmdaa.ui.dispatcher.{Dispatcher, StateAction}
import org.scalajs.dom.Event
import org.scalajs.dom.raw.HTMLInputElement

package object forms {

  /**
   * DOM Event Handler for keeping Application state up-to-date with text-based
   * form elements.
   *
   * @param action     a function that produces a StateAction for changed text
   * @param event      the original HTML Event
   * @param dispatcher the Dispatcher for distributing StateActions
   */
  def handleInput(action: String => StateAction)(event: Event)
    (implicit dispatcher: Dispatcher)
  : Unit = {
    event.currentTarget match {
      case input: HTMLInputElement =>
        dispatcher dispatch action(input.value)
      case _ =>
    }
  }
}
