/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.forms

import cmdaa.ui.components.FormInput
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.state.RerunFormActions._
import cmdaa.ui.state.RerunFormStateRO
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding.{Var, Vars}
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Event
import org.scalajs.dom.raw._

object RerunForm {
  @html
  def component(rerunFormState: RerunFormStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    <form id="rerunForm"
      onsubmit={ e: Event =>
        e.preventDefault()
        dispatcher dispatch RerunFormSubmit
      }
    >
      {
        FormInput.doubleField (
          "rerunLlThreshold",
          "LL Threshold",
          rerunFormState.llThreshold,
          LlThresholdChanged.apply,
          Vars.empty,

          placeholder = Some("0.06"),
          max  = Some(1.0),
          min  = Some(0.0),
          step = Some(0.01),
          required = true,

          leftIcon = Var(None)
        ).bind
      } {
        FormInput.doubleField (
          "rerunCosThreshold",
          "Cos Threshold",
          rerunFormState.cosThreshold,
          CosThresholdChanged.apply,
          Vars.empty,

          placeholder = Some("0.9"),
          max  = Some(1.0),
          min  = Some(0.0),
          step = Some(0.01),
          required = true,

          leftIcon = Var(None)
        ).bind
      }

      <input type="submit" style="display: none" />
    </form>
  }
}
