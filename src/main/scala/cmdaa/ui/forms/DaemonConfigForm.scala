/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.forms

import cmdaa.ui.AppActions.LogoutAction
import cmdaa.ui.components.FormInput
import cmdaa.ui.dao.{GrokstoreDao, HttpError, UnauthenticatedError}
import cmdaa.ui.dispatcher._
import cmdaa.ui.forms.DaemonConfigForm.actions.{RemNodeLbl, RemPath}
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.{BindingSeq, Var, Vars}
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.{CmdaaLog, DsConfig}

import scala.collection.mutable

object DaemonConfigForm {

  object actions {
    /** Text changed in the DaemonPrefix field */
    case class DaemonPrefixChanged(txt: String) extends StateAction

    /** Text changed in the Path Edit field */
    case class EdtPathChanged(txt: String) extends StateAction

    /** Path requested to be added. */
    case class AddPath(path: String) extends StateAction

    /** Path requested to be removed.  */
    case class RemPath(path: String) extends StateAction

    /** Paths requested to be cleared. */
    case object ClearPaths extends StateAction

    /** Text changed in the NodeLabel Edit field */
    case class EdtNodeLblChanged(txt: String) extends StateAction

    /** NodeLbl requested to be added */
    case class AddNodeLbl(nodeLbl: String) extends StateAction

    /** NodeLbl requested to be removed. */
    case class RemNodeLbl(nodeLbl: String) extends StateAction

    /** NodeLbls requested to be cleared. */
    case object ClearNodeLbls extends StateAction

    /** DsConfigForm Submitted */
    case object FormSubmit extends StateAction

    /** DsConfigForm Cleared (just the edit fields) */
    case object FormClear extends StateAction
  }

  object component {
    import cmdaa.ui.utils.intellijBindingHelper
    import org.lrng.binding.html
    import org.lrng.binding.html.NodeBinding
    import org.scalajs.dom.Event
    import org.scalajs.dom.raw._

    @html
    def apply(state: StateRO)
      (implicit dispatcher: Dispatcher)
    : NodeBinding[Node] = {
      val paths: Binding[List[String]] = Binding {
        val remdPaths = state.remdPaths.all.bind
        state.cmdaaLog.bind
          .flatMap(_.dsConfig.map(_.paths.toList))
          .getOrElse(Nil)
          .appendedAll(state.paths.all.bind.toList)
          .filter(!remdPaths.contains(_))
      }

      val nodeLbls: Binding[List[String]] = Binding {
        val remdNodeLbls = state.remdNodeLbls.all.bind
        state.cmdaaLog.bind
          .flatMap(_.dsConfig.map(_.nodeLbls.toList))
          .getOrElse(Nil)
          .appendedAll(state.nodeLbls.all.bind.toList)
          .filter(!remdNodeLbls.contains(_))
      }

      <form
        id="daemonsetConfigForm"
        autocomplete="off"
        onsubmit={ e: Event =>
          e.preventDefault()
          dispatcher dispatch actions.FormSubmit
        }
      >
        {
          FormInput.textField (
            "edtDaemonsetPrefix",
            "DaemonSet Prefix",
            state.daemonPrefix,
            actions.DaemonPrefixChanged.apply,
            Vars.empty,

            placeholder = Some("my_daemon"),
            leftIcon    = Var(Some("fas fa-file")),
            pattern     = Var(Some("""[A-Za-z_][0-9A-Za-z\-_]*""")),
            minLength   = Some(1),
            maxLength   = Some(100),
            required    = true
          ).bind
        } {
          if (paths.bind.isEmpty && nodeLbls.bind.isEmpty) {
            <!-- No paths or labels -->
          } else {
            <div class="tile is-ancestor">
              <div class="tile is-12 is-parent">
                <div class="tile is-child box">{
                  if (paths.bind.isEmpty) {
                    <!-- No paths -->
                  } else {
                    <div>
                      <strong>Paths:</strong>
                      <div class="tags">{
                        paths.bind.map { path =>
                          <span class="tag">
                            { path }
                            <button
                              class="delete is-small"
                              onclick={ e: Event =>
                                e.preventDefault()
                                dispatcher dispatch RemPath(path)
                              }
                            ></button>
                          </span>
                        }
                      }</div>
                    </div>
                  }
                } {
                  if (nodeLbls.bind.isEmpty) {
                    <!-- No NodeLbls -->
                  } else {
                    <div>
                      <strong>Node Labels:</strong>
                      <div class="tags">{
                        nodeLbls.bind.map { nodeLbl =>
                          <span class="tag">
                            { nodeLbl }
                            <button
                              class="delete is-small"
                              onclick={ e: Event =>
                                e.preventDefault()
                                dispatcher dispatch RemNodeLbl(nodeLbl)
                              }
                            ></button>
                          </span>
                        }
                      }</div>
                    </div>
                  }
                }</div>
              </div>
            </div>
          }
        } {
          FormInput.textActionField (
            "edtPath", "Path",
            state.edtPath,
            Var("Add"),
            Vars.empty,

            actions.EdtPathChanged.apply,
            actions.AddPath.apply,

            placeholder = Some("/path/to/logfile.log"),
            leftIcon    = Var(Some("fas fa-file")),
            pattern     = Var(Some("^/|(/[\\w-]+)+$"))
          ).bind
        } {
          FormInput.textActionField (
            "edtNodeLbl", "Node Label",
            state.edtNodeLbl,
            Var("Add"),
            Vars.empty,

            actions.EdtNodeLblChanged.apply,
            actions.AddNodeLbl.apply,

            placeholder = Some("HDFS"),
            leftIcon    = Var(Some("fas fa-tag"))
          ).bind
        }
      </form>
    }
  }

  /**
   * The DaemonSet Config Form's state.
   *
   * @param cmdaaLog the selected CMDAA Log to define DaemonSet Config for
   */
  case class State(cmdaaLog: Var[Option[CmdaaLog]])
  extends StateModel[StateRO] {
    val daemonPrefix: Var[String] = Var("")
    val edtPath:      Var[String] = Var("")
    val edtNodeLbl:   Var[String] = Var("")

    val paths:    Vars[String] = Vars.empty
    val nodeLbls: Vars[String] = Vars.empty

    val remdPaths:    Vars[String] = Vars.empty
    val remdNodeLbls: Vars[String] = Vars.empty

    override def toRO: StateRO =
      StateRO(cmdaaLog,
        daemonPrefix,
        edtPath, edtNodeLbl,
        paths, nodeLbls,
        remdPaths, remdNodeLbls)

    def clear(): Unit = {
      edtPath.value    = ""
      edtNodeLbl.value = ""
    }
  }

  /** ReadOnly version of DaemonSet Config form data */
  case class StateRO (
    cmdaaLog:     Binding[Option[CmdaaLog]],
    daemonPrefix: Binding[String],
    edtPath:      Binding[String],
    edtNodeLbl:   Binding[String],
    paths:        BindingSeq[String],
    nodeLbls:     BindingSeq[String],
    remdPaths:    BindingSeq[String],
    remdNodeLbls: BindingSeq[String]
  ) extends StateModelRO

  class Handler (
    private val state:     State,
    private val sessToken: Var[SessionToken]
  ) extends ActionHandler {
    import actions._
    import cmdaa.ui.state.AuthenticatedActions.{EditDaemonSet, DsConfigOpen}

    import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

    override def handle(dispatcher: Dispatcher)
    : PartialFunction[StateAction, Unit] = {
      case EditDaemonSet => hndlEditDaemonSetReq()

      case DaemonPrefixChanged(txt) => state.daemonPrefix.value = txt
      case EdtPathChanged(txt)      => state.edtPath.value = txt
      case AddPath(txt)             => hndlAddPath(txt)
      case RemPath(path)            => hndlRemPath(path)
      case ClearPaths               => state.paths.value.clear()

      case EdtNodeLblChanged(txt) => state.edtNodeLbl.value = txt
      case AddNodeLbl(txt)        => hndlAddNodeLbl(txt)
      case RemNodeLbl(nodeLbl)    => hndlRemNodeLbl(nodeLbl)
      case ClearNodeLbls          => state.nodeLbls.value.clear()

      case FormSubmit => hndlFormSubmit(dispatcher)
      case FormClear  => state.clear()

      case DsConfigOpen(false) => state.clear()
      case _ =>
    }

    /** Handles EditDaemonSet request */
    private def hndlEditDaemonSetReq(): Unit = {
      state.cmdaaLog.value
        .flatMap(_.dsConfig)
        .foreach { dsConfig =>
          state.daemonPrefix.value = dsConfig.daemonPrefix.getOrElse("")

          state.paths       .value.clear()
          state.nodeLbls    .value.clear()
          state.remdPaths   .value.clear()
          state.remdNodeLbls.value.clear()
        }
    }

    private def hndlFormSubmit(dispatcher: Dispatcher): Unit = {
      for (cmdaaLog <- state.cmdaaLog.value) yield {
        val paths =
          (cmdaaLog.dsConfig.map(_.paths).getOrElse(Array())
            ++ state.paths.value.toArray)
          .filter(!state.remdPaths.value.toArray.contains(_))

        val nodeLbls =
          (cmdaaLog.dsConfig.map(_.nodeLbls).getOrElse(Array())
            ++ state.nodeLbls.value.toArray)
          .filter(!state.remdNodeLbls.value.toArray.contains(_))

        GrokstoreDao.setDaemonSetConfig (
          sessToken.value, cmdaaLog.id,
          DsConfig (
            paths, nodeLbls,
            Some(state.daemonPrefix.value)
          )
        ).onComplete (
          _.fold({
            case err: HttpError =>
              System.err.println (
                s"""Unhandled HttpError Setting LogLocation in GrokStore:
                   |${err.status}/${err.sText} -
                   |${err.rText}"""
                  .stripMargin.replaceAll("\n", "")
              )
            case UnauthenticatedError =>
              dispatcher dispatch LogoutAction
          }, { cmdaaLog =>
            // TODO: Update CmdaaLog store with updated CmdaaLog
          })
        )
      }

      dispatcher dispatch DsConfigOpen(false)
    }

    private def hndlAddPath(txt: String): Unit = {
      val path = txt.trim
      state.cmdaaLog.value.foreach { log =>
        if (log.dsConfig.exists(_.paths.contains(path))) {
          state.edtPath.value = ""
          removeFrom(path, state.remdPaths.value)
        } else if (appendTo(path, state.paths.value)) {
          state.edtPath.value = ""
        }
      }
    }

    private def hndlAddNodeLbl(txt: String): Unit = {
      val nodeLbl = txt.trim
      state.cmdaaLog.value.foreach { log =>
        if (log.dsConfig.exists(_.nodeLbls.contains(nodeLbl))) {
          state.edtNodeLbl.value = ""
          removeFrom(nodeLbl, state.remdNodeLbls.value)
        } else if (appendTo(nodeLbl, state.nodeLbls.value)) {
          state.edtNodeLbl.value = ""
        }
      }
    }

    private def hndlRemPath(txt: String): Unit = {
      val value = txt.trim
      if (value.isEmpty) return

      if (!removeFrom(value, state.paths.value))
        state.remdPaths.value += value
    }

    private def hndlRemNodeLbl(txt: String): Unit = {
      val value = txt.trim
      if (value.isEmpty) return

      if (!removeFrom(value, state.nodeLbls.value))
        state.remdNodeLbls.value += value
    }

    private def appendTo(value: String, buffer: mutable.Buffer[String])
    : Boolean = {
      if (value.nonEmpty && !buffer.contains(value)) {
        buffer.append(value)
        true
      } else false
    }

    private def removeFrom(value: String, buffer: mutable.Buffer[String])
    : Boolean = {
      if (buffer.contains(value)) {
        buffer.subtractOne(value)
        true
      } else false
    }
  }
}
