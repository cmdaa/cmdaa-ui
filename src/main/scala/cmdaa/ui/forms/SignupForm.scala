/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.forms

import cmdaa.ui.components.FormInput
import cmdaa.ui.dispatcher.Dispatcher
import cmdaa.ui.state.SignupActions._
import cmdaa.ui.state.SignupStateRO
import cmdaa.ui.utils.intellijBindingHelper
import com.thoughtworks.binding.Binding._
import org.lrng.binding.html
import org.lrng.binding.html.NodeBinding
import org.scalajs.dom.Event
import org.scalajs.dom.raw._

object SignupForm {

  /**
   * Constructs the SignupForm component.
   *
   * @param signupState ReadOnly SignupState for this Form
   * @param dispatcher  Action Dispatcher
   *
   * @return Element Binding for the SignupForm
   */
  @html
  def component(signupState: SignupStateRO)
    (implicit dispatcher: Dispatcher)
  : NodeBinding[Node] = {
    <form id="signupForm"
      onsubmit={ e: Event =>
        e.preventDefault()
        dispatcher dispatch SignupFormSubmit
      }
    >
      {
        FormInput.textField (
          "signupUsername",
          "User Name",
          signupState.username,
          SignupUsernameChanged.apply,
          Vars.empty,

          placeholder = Some("Desired Username..."),
          pattern     = Var(Some("""[0-9A-Za-z][0-9A-Za-z.\-_]+""")),
          minLength   = Some(3),
          maxLength   = Some(100),
          required    = true,
          leftIcon    = Var(Some("fas fa-user")),

          readonly = signupState.username_ro
        ).bind
      }
      {
        if (!signupState.pkiEnabled.bind) {
          <div>
            {
              FormInput.textField (
                "signupPassword",
                "Password",
                signupState.password,
                SignupPasswordChanged.apply,
                Vars.empty,

                fieldType = "password",
                required  = true,
                minLength = Some(3),
                maxLength = Some(100),
                leftIcon  = Var(Some("fas fa-key"))
              ).bind
            } {
              FormInput.textField(
                "signupVerifyPwd",
                "Password (Again)",
                signupState.verifyPwd,
                SignupVerifyPwdChanged.apply,
                Vars.empty,

                fieldType = "password",
                required  = true,
                minLength = Some(3),
                maxLength = Some(100),
                leftIcon  = Var(Some("fas fa-key"))
              ).bind
            }
          </div>.bind
        } else {
          <!-- Pki Authenticated -->.bind
        }
      }
      {
        FormInput.textField (
          "signupEmail",
          "Email",
          signupState.email,
          SignupEmailChanged.apply,
          Vars.empty,

          fieldType   = "email",
          required    = true,
          placeholder = Some("me@domain.tld"),
          leftIcon    = Var(Some("fas fa-envelope")),

          readonly = signupState.email_ro
        ).bind
      } {
        FormInput.textField (
          "signupFirstName",
          "First Name",
          signupState.firstName,
          SignupFirstNameChanged.apply,
          Vars.empty,

          maxLength   = Some(100),
          placeholder = Some("Firstname"),
          leftIcon    = Var(Some("fas fa-signature")),

          readonly = signupState.firstName_ro
        ).bind
      } {
        FormInput.textField (
          "signupLastName",
          "Last Name",
          signupState.lastName,
          SignupLastNameChanged.apply,
          Vars.empty,

          placeholder = Some("Lastname"),
          maxLength   = Some(100),
          leftIcon    = Var(Some("fas fa-signature")),

          readonly = signupState.lastName_ro
        ).bind
      }

      <input type="submit" style="display: none" />
    </form>
  }
}
