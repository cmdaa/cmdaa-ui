/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.forms

import cmdaa.ui.AppActions.LogoutAction
import cmdaa.ui.dao.{GrokstoreDao, HttpError, UnauthenticatedError}
import cmdaa.ui.dispatcher._
import cmdaa.ui.forms.EditGrokForm.data.GrokSelection
import com.thoughtworks.binding.Binding
import com.thoughtworks.binding.Binding.{Constants, Var}
import io.cmdaa.auth.api.SessionToken
import io.cmdaa.gs.api.{CmdaaLog, Grok, GrokRun}

object EditGrokForm {
  /** Actions emitted by the EditGrokForm component */
  object actions {
    /** Action emitted on Form.Submit. */
    case object FormSubmit extends StateAction

    /** A field was selected */
    case class FieldSelected(gf: data.GrokField) extends StateAction

    /** Grok fieldname changed event. */
    case class GrokFieldnameChanged(txt: String) extends StateAction

    /** Apply action requested. */
    case class ApplyChanges(gf: data.GrokField) extends StateAction

    /** Revert action requested. */
    case class RevertChanges(gf: data.GrokField) extends StateAction
  }

  /** EditGrokForm Component: displays state; emits actions */
  object component {
    import actions._
    import cmdaa.ui.utils.{intellijBindingHelper, intellijNodeSeqHelper}
    import data.GrokField
    import org.lrng.binding.html
    import org.lrng.binding.html.{NodeBinding, NodeBindingSeq}
    import org.scalajs.dom.Event
    import org.scalajs.dom.raw.Node

    @html
    def apply(state: StateRO)
      (implicit dispatcher: Dispatcher)
    : NodeBinding[Node] = {
      <form
        id="grokEditForm"
        autocomplete="off"
        onsubmit={ e: Event =>
          e.preventDefault()
          dispatcher dispatch actions.FormSubmit
        }
      >{
        state.grokSel.bind.fold[NodeBindingSeq[Node]] {
          Constants.empty[Node]
        } { _ =>
          val v_grokStr = state.mutGrok.grok
          <div class="tile is-ancestor">
            <div class="tile is-12 is-parent">
              <div class="tile is-child box">
                <strong>Grok:</strong>
                <span class="is-size-7">{v_grokStr.bind}</span>
              </div>
            </div>
          </div>
          <div class="tile is-ancestor">
            <div class="tile is-4 is-parent">
              <div class="tile is-child box">
                { fieldMenu(state) }
              </div>
            </div>
            <div class="tile is-8 is-parent">
              <div id="grokFieldEditTile" class="tile is-child box">
                <p class="title">Grok Field</p>
                {
                  state.selField.bind
                    .fold[NodeBindingSeq[Node]] {
                      Constants[Node](
                        <p>Select a field on the left.</p>.bind
                      )
                    } { selFld =>
                      fieldControls(state, selFld)
                    }
                }
              </div>
            </div>
          </div>
        }
      }</form>
    }

    /** Grok Fields Menu */
    @html
    private def fieldMenu(state: StateRO)
      (implicit dispatcher: Dispatcher)
    : NodeBinding[Node] =
      <div id="fieldMenu" class="menu">
        <p class="menu-label">Fields:</p>
        <ul class="menu-list">
          {
            for (gf <- state.fields.bind) yield {
              val linkClass = Binding {
                state.selField.bind.fold("") { sgf =>
                  if (gf == sgf) "is-active" else ""
                }
              }

              <li>
                <a href="#" class={ linkClass.bind }
                  onclick={ selectGrokField(gf)(_) }
                >{gf.fieldType}:{gf.fieldName}</a>
              </li>
            }
          }
        </ul>
      </div>

    /** Grok Edit Field Controls */
    @html
    private def fieldControls(state: StateRO, gf: data.GrokField)
      (implicit dispatcher: Dispatcher)
    : NodeBindingSeq[Node] = {
      <div class="field">
        <label class="label" for="fieldNameTxt">Field Name:</label>
        <div class="control">
          <input
            id="fieldNameTxt" class="input" type="text"
            placeholder="FieldName"
            pattern="[A-Za-z_][0-9A-Za-z_]+"
            value={ state.fieldnameValue.bind }
            oninput={ handleInput(GrokFieldnameChanged.apply)(_) }
          />
        </div>
      </div>
      <div class="field">
        <label class="label" for="fieldTypeTxt">Field Type:</label>
        <div class="control">
          <input
            id="fieldTypeTxt" class="input" type="text"
            placeholder="FieldType"
            readonly="readonly"
            value={ state.fieldtypeValue.bind }
          />
        </div>
      </div>
      <div class="field is-grouped is-grouped-right">
        <div class="control">
          <button
            class="button is-link"
            onclick={ applyChanges(gf)(_) }
          >Apply</button>
        </div>
        <div class="control">
          <button
            class="button is-link is-light"
            onclick={ revertChanges(gf)(_) }
          >Revert</button>
        </div>
      </div>
    }

    /** Event handler for selecting GrokFields, from the Field Menu */
    private def selectGrokField(gf: GrokField)(e: Event)
      (implicit dispatcher: Dispatcher)
    : Unit = {
      e.preventDefault()
      dispatcher dispatch actions.FieldSelected(gf)
    }

    /** Event handler for apply changes to a GrokField */
    private def applyChanges(gf: GrokField)(e: Event)
      (implicit dispatcher: Dispatcher)
    : Unit = {
      e.preventDefault()
      dispatcher dispatch ApplyChanges(gf)
    }

    /** Event Handler for the "Revert" button. */
    private def revertChanges(gf: GrokField)(e: Event)
      (implicit dispatcher: Dispatcher)
    : Unit = {
      e.preventDefault()
      dispatcher dispatch RevertChanges(gf)
    }
  }

  /** Custom data types for the EditGrokForm */
  object data {
    /** Holds onto to parsed-out field information */
    case class GrokField(fieldType: String, fieldName: String)

    /** Container of Grok Selection data. */
    case class GrokSelection(grokRun: GrokRun, grok: Grok, grokIdx: Int)

    /** Mutable data of the edited Grok. */
    case class MutGrok (
      grok:   Var[String]  = Var(""),
      active: Var[Boolean] = Var(true)
    ) {
      def clear(): Unit = {
        grok.value   = ""
        active.value = true
      }
    }
  }

  /** Mutable state of the EditGrokForm */
  case class State (
    cmdaaLog: Var[Option[CmdaaLog]],
    grokSel:  Var[Option[GrokSelection]]  = Var(None),
    selField: Var[Option[data.GrokField]] = Var(None),

    mutGrok: data.MutGrok = data.MutGrok(),

    fieldnameValue: Var[String] = Var(""),
    fieldtypeValue: Var[String] = Var("")
  ) extends StateModel[StateRO] {
    override def toRO: StateRO = StateRO (
      cmdaaLog, grokSel, selField,
      mutGrok,
      fieldnameValue, fieldtypeValue
    )

    /** Clears the EditGrokForm state. */
    def clear(): Unit = {
      grokSel.value  = None
      selField.value = None

      fieldnameValue.value = ""
      fieldtypeValue.value = ""
    }
  }

  /** Immutable state of the EditGrokForm (given to component) */
  case class StateRO (
    cmdaaLog: Binding[Option[CmdaaLog]],
    grokSel:  Binding[Option[GrokSelection]],
    selField: Binding[Option[data.GrokField]],

    mutGrok: data.MutGrok,

    fieldnameValue: Binding[String],
    fieldtypeValue: Binding[String]
  ) extends StateModelRO {
    import data.GrokField

    /** The pattern for finding GrokFields */
    private val FieldPattern = """%\{([a-zA-Z \-_]+):([a-zA-Z \-_0-9]+)}""".r

    /** The parsed GrokFields from the Grok text. */
    val fields: Binding[List[GrokField]] = Binding {
      FieldPattern.findAllIn(mutGrok.grok.bind).matchData
        .map(m => GrokField(m.group(1), m.group(2)))
        .toList
    }
  }

  /** ActionHandler for EditGrokForm */
  class Handler (
    private val state: State,
    private val sessToken: Var[SessionToken]
  ) extends ActionHandler {
    import actions._
    import cmdaa.ui.state.AuthenticatedActions.{EditGrok, GeditModalOpen}
    import data.GrokField
    import org.scalajs.dom.document
    import org.scalajs.dom.raw.HTMLInputElement

    import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

    override def handle(dispatcher: Dispatcher)
    : PartialFunction[StateAction, Unit] = {
      case EditGrok(_, grokRun, grokIdx, grok) =>
        hndlEditGrokReq(grokRun, grok, grokIdx)

      case FieldSelected(gf) =>
        if (state.selField.value.contains(gf))
          hndlFieldUnselected()
        else
          hndlFieldSelected(gf)

      case ApplyChanges(gf)  => hndlApplyChanges(gf)
      case RevertChanges(gf) => hndlRevertChanges(gf)
      case FormSubmit        => hndlFormSubmit()(dispatcher)

      case GeditModalOpen(false)     => state.clear()
      case GrokFieldnameChanged(txt) => state.fieldnameValue.value = txt

      case _ =>
    }

    /** Setup the EditGrokForm with the selected Grok. */
    private def hndlEditGrokReq(grokRun: GrokRun, grok: Grok, grokIdx: Int)
    : Unit = {
      state.grokSel.value = Some(GrokSelection(grokRun, grok, grokIdx))
      state.mutGrok.grok.value = grok.grok
    }

    /** Send Grok changes to the Grokstore. */
    private def hndlFormSubmit()(dispatcher: Dispatcher): Unit = {
      for {
        cmdaaLog <- state.cmdaaLog.value
        grokSel  <- state.grokSel.value
      } yield {
        GrokstoreDao.updateGrok (
          sessToken.value,
          cmdaaLog.id, grokSel.grokRun, grokSel.grokIdx,
          state.mutGrok.grok.value
        ).onComplete (
          _.fold({
            case err: HttpError =>
              System.err.println (
                s"""Unhandled HttpError Deleting Run from GrokStore:
                   |${err.status}/${err.sText} -
                   |${err.rText}"""
                  .stripMargin.replaceAll("\n", "")
              )
            case UnauthenticatedError =>
              dispatcher dispatch LogoutAction
          }, { cmdaaLog =>
            //TODO: Update CmdaaLog store with updated CmdaaLog.
          })
        )
      }

      dispatcher dispatch GeditModalOpen(false)
    }

    /** Handle FieldSelected event */
    private def hndlFieldSelected(gf: GrokField): Unit = {
      state.selField.value = Some(gf)
      state.fieldnameValue.value = gf.fieldName
      state.fieldtypeValue.value = gf.fieldType
      focusFieldnameTxt()
    }

    /** Handles FieldUnselected event */
    private def hndlFieldUnselected(): Unit = {
      state.selField.value = None
      state.fieldnameValue.value = ""
      state.fieldtypeValue.value = ""
    }

    /** Handle ApplyChanges event */
    private def hndlApplyChanges(gf: GrokField): Unit = {
      val origFieldName = gf.fieldName
      val origFieldType = gf.fieldType

      val newName = state.fieldnameValue.value
      val newType = state.fieldtypeValue.value

      if (origFieldName == newName && origFieldType == newType) {
        println("Nothing changed.")
        return
      }

      val mGrokStr = state.mutGrok.grok
      mGrokStr.value = mGrokStr.value.replaceAll (
        s"""%{$origFieldType:$origFieldName}""",
        s"""%{$newType:$newName}"""
      )
    }

    /** Handle RevertChanges event */
    private def hndlRevertChanges(gf: GrokField): Unit = {
      state.fieldnameValue.value = gf.fieldName
      state.fieldtypeValue.value = gf.fieldType
      focusFieldnameTxt()
    }

    /** Utility for focusing the "fieldNameTxt" Input */
    private def focusFieldnameTxt(): Unit = {
      Option(document.getElementById("fieldNameTxt"))
        .foreach { case input: HTMLInputElement => input.focus() }
    }
  }
}
