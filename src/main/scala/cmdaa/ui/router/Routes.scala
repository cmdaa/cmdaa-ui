/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui.router

import cmdaa.ui.dispatcher.{Dispatcher, StateModelRO}
import com.thoughtworks.binding
import com.thoughtworks.binding.Binding
import org.scalajs.dom.Node

/**
 * Naive implementation of a nestable Routing component.
 *
 * @param currRoute The Single-Page-Appliction's current route
 * @param notFound  What to display when the current route isn't defined
 * @param routeMap  Map of route path names to components
 * @param rootUri   The Root URI of this Router
 *
 * @tparam T the type of ReadOnly StateModel to pass to page components
 */
class Routes[T <: StateModelRO] (
  val currRoute: Binding[String],
  val notFound:  Route[T],
  val routeMap:  Map[String, Route[T]],
  val rootUri:   String = ""
) {
  def route(appState: T)(implicit dispatcher: Dispatcher)
  : binding.Binding.F[Node] =
    Binding {
      routeMap
        .getOrElse(rootUri+currRoute.bind, notFound)
        .component(appState)
        .bind
    }
}

object Routes {
  def apply[T <: StateModelRO] (
    currRoute: Binding[String],
    notFound:  Route[T]
  )(routes: (String, Route[T])*)
  : Routes[T] =
    new Routes(currRoute, notFound, routes.toMap)

  def apply[T <: StateModelRO] (
    rootUri:   String,
    currRoute: Binding[String],
    notFound:  Route[T]
  )(routes: (String, Route[T])*)
  : Routes[T] =
    new Routes(currRoute, notFound, routes.toMap, rootUri)
}
