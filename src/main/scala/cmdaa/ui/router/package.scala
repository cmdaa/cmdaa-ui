/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmdaa.ui

import cmdaa.ui.dispatcher.{Dispatcher, StateModelRO}
import com.thoughtworks.binding.Binding
import org.scalajs.dom.{Event, Node, window}

package object router {

  /** Defines a Route with the Single-Page-App */
  trait Route[T <: StateModelRO] {
    /**
     * Constructs the component to be displayed at a particular route.
     *
     * @param stateModel ReadOnly StateModel to be Rendered
     * @param dispatcher Action Dispatcher
     *
     * @return the page component
     */
    def component(stateModel: T)(implicit dispatcher: Dispatcher)
    : Binding[Node]
  }

  /**
   * DOM EventHandler for linking to a route, rather than a normal URL.
   * Prevents full page refresh when clicking internal/application links.
   *
   * @param pathname   The path of the route to link to.
   * @param e          The triggering DOM Event
   * @param dispatcher ActionDispatcher
   */
  def navLink(pathname: String)(e: Event)
    (implicit dispatcher: Dispatcher)
  : Unit = {
    e.preventDefault()

    window.location.origin foreach { origin =>
      window.history.pushState(null, "", origin + pathname)
    }

    dispatcher dispatch AppActions.PathUpdate
  }
}
