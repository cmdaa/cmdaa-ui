#!/bin/bash

TAG=0.0.23

sbt build

eval $(minikube docker-env)
docker build -t cmdaa/cmdaa-ui:${TAG} .

kubectl -n cmdaa rollout restart deploy cmdaa-ui
exec kubectl -n cmdaa get pods --watch
