const { merge }       = require('webpack-merge');
const core            = require('./webpack-core.config.js');
const generatedConfig = require("./scalajs.webpack.config.js");
const userConfig      = require("./user.config.js");

const entries = {};
entries[Object.keys(generatedConfig.entry)[0]] = "scalajs";

module.exports = merge(core, {
  devtool: "cheap-module-eval-source-map",

  devServer: {
    https: true,
    historyApiFallback: true,
    disableHostCheck:   true,

    proxy: {
      '/auth': {
        changeOrigin: true,
        //target: 'https://cmdaa-auth.' + userConfig.serviceDN + ':443',
        target: 'http://localhost:8080/',
        pathRewrite: { '^/auth': '' },
        secure: false
      },
      '/wf': {
        changeOrigin: true,
        target: 'https://cmdaa-workflows.' + userConfig.serviceDN + ':443/',
        pathRewrite: { '^/wf': '' },
        secure: false
      },
      '/gs': {
        changeOrigin: true,
        //target: 'https://grokstore.' + userConfig.serviceDN + ':443/',
        target: 'http://localhost:9080/',
        pathRewrite: { '^/gs': '' },
        secure: false
      }
    }
  },

  entry: entries,

  module: {
    noParse: (content) => {
      return content.endsWith("-fastopt.js");
    },
    rules: [{
      test: /\-fastopt.js$/,
      use: [ require.resolve('./fastopt-loader.js') ]
    }]
  }
});
